package com.datafoundry.raven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

import com.datafoundry.raven.config.StringTypeDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * @author akhilesh
 * @created on 18/12/2020
 * @description Class for starting the application
 * @version number Raven v1.0
 */

@SpringBootApplication
@EnableAsync
public class RavenServiceApplication {

	@Bean
	public SimpleModule injectDeser() {
		return new SimpleModule().addDeserializer(String.class, new StringTypeDeserializer());
	}

	public static void main(String[] args) {
		SpringApplication.run(RavenServiceApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
