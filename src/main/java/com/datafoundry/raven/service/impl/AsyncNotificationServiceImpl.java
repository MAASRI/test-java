package com.datafoundry.raven.service.impl;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import com.datafoundry.raven.exception.AppException;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.raven.entity.ChannelModel;
import com.datafoundry.raven.entity.NotificationModel;
import com.datafoundry.raven.models.Notification;
import com.datafoundry.raven.models.User;
import com.datafoundry.raven.models.UserGroupRequest;
import com.datafoundry.raven.models.UserGroupResponse;
import com.datafoundry.raven.models.UserSession;
import com.datafoundry.raven.models.ChannelUpdate;
import com.datafoundry.raven.repositories.MongoRepository;
import com.datafoundry.raven.service.AsyncNotificationService;
import com.datafoundry.raven.service.ChannelService;
import com.datafoundry.raven.utils.AppConstants;
import com.datafoundry.raven.utils.RequestType;
import com.datafoundry.raven.utils.Utility;

import static com.datafoundry.raven.utils.AppConstants.DELETED;
import static com.datafoundry.raven.utils.AppConstants.STATUS;
import static com.mongodb.client.model.Updates.set;

/**
 * @author srilakshmi
 * @version number Raven v1.0
 * @created 30/12/2020
 * @description This component is used for Async methods
 */
@Component
public class AsyncNotificationServiceImpl implements AsyncNotificationService {

	RRAdapter logger = new RRAdapterImpl(AsyncNotificationServiceImpl.class.getName());

	@Autowired
	MongoRepository<NotificationModel> mongoRepository;

	@Autowired
	RestService restService;
	@Lazy
	@Autowired
	ChannelService channelService;

	@Value("${user.service.get.user.url}")
	String userServiceGetUserUrl;

	@Value("${channel.notification.expiry.hours}")
	Long channelNotificationExpiryHours;

	@Async
	@Override
	public void updateNotificationStatusByListOfIds(Bson filter, Bson update, String clientId,
			String notificationCollection, Class<NotificationModel> typeOfClass, String rrId) {
		try {
			logger.info(rrId, AppConstants.RAVEN, "update Notification Status By ListOfIds", null);
			mongoRepository.updateMany(filter, update, clientId, AppConstants.NOTIFICATION_COLLECTION,
					NotificationModel.class);
		} catch (Exception ex) {
			logger.error(rrId, AppConstants.RAVEN, Utility.getExceptionMessage(ex), null);
		}

	}

	@Async
	@Override
	public void updateAllNotificationStatusByChannelId(UserSession userSession, String channelId, String status, String rrId) {
		try {
			logger.info(rrId, AppConstants.RAVEN, "Updating the status for a given " +channelId+ " to " +status, null);
			Bson whereQuery = Filters.and(Filters.eq(AppConstants.CHANNEL_ID, channelId),
					Filters.ne(AppConstants.STATUS, AppConstants.DELETED));

			Bson updateQuery = Updates.combine(Updates.set(AppConstants.STATUS, status),
					Updates.set(AppConstants.UPDATED_AT, LocalDateTime.now(ZoneOffset.UTC)),
					Updates.set(AppConstants.UPDATED_BY, userSession.getUserId()));
			mongoRepository.updateMany(whereQuery, updateQuery, userSession.getClientId(), AppConstants.NOTIFICATION_COLLECTION, NotificationModel.class);
		} catch (Exception ex) {
			logger.error(rrId, AppConstants.RAVEN, Utility.getExceptionMessage(ex), null);
			throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
	}

	@Override
	@Async
	public void createChannelNotifications(Notification notification, ChannelModel channel) throws InterruptedException {
		Set<String> userIds = new TreeSet<>();
		try {
			logger.info(notification.getRrId(), AppConstants.RAVEN, "Creating Channel Notifications", null);
				if (channel.getUsers() != null && !channel.getUsers().isEmpty())
					userIds.addAll(channel.getUsers());
				if (channel.getUserGroup() != null && !channel.getUserGroup().isEmpty())
					userIds.addAll(getUserGroupUserIds(channel.getUserGroup(), notification));
				logger.info(notification.getRrId(), AppConstants.RAVEN,
						"creating notification records for users in channel group", null);
				if (!userIds.isEmpty())
					saveNotificationsForUserIds(notification, userIds);

		} catch (Exception ex) {
			logger.error(notification.getRrId(), AppConstants.RAVEN,
					"Failed to create documents for User groups " + ex.getMessage(), null);
		}
	}

	public Set<String> getUserGroupUserIds(List<String> groupIds, Notification notification) {
		Set<String> userIds = new TreeSet<>();
		try {
			logger.info(notification.getRrId(), AppConstants.RAVEN, "Creating Notifications for User Groups", null);
			userIds = getUserIdsForGroups(groupIds, notification.getClientId(),notification.getRrId());
		} catch (Exception ex) {
			logger.error(notification.getRrId(), AppConstants.RAVEN,
					"Failed to create documents for User groups " + ex.getMessage(), null);
		}
		return userIds;
	}

	private Set<String> getUserIdsForGroups(List<String> groupIds, String clientId,String rrId) {
		Set<String> userIds = new TreeSet<>();
		UserGroupRequest userGroupRequest = new UserGroupRequest();
		userGroupRequest.setClientId(clientId);
		userGroupRequest.setUserGroups(groupIds);
		ResponseEntity<UserGroupResponse> response = restService.getResponse(rrId,
				userServiceGetUserUrl, RequestType.GET_USER_IDS, userGroupRequest, null, UserGroupResponse.class);
		response.getBody().getData().forEach(data -> {
			userIds.addAll(data.listOfUsers.stream().map(User::getUserId).collect(Collectors.toSet()));
		});
		return userIds;
	}

	@Override
	public Boolean saveNotificationsForUserIds(Notification notification, Set<String> userIds) {
		Boolean result = false;
		try {
			logger.info(notification.getRrId(), AppConstants.RAVEN, "Creating records for UserIds", null);
			List<NotificationModel> notificationRecords = userIds.stream().map(id -> setUserId(notification, id))
					.map(n -> Utility.createNotificationDBModel(n, String.valueOf(n.getUserIds().get(0)), channelNotificationExpiryHours))
					.collect(Collectors.toList());
			result = mongoRepository.saveMany(notificationRecords, notification.getClientId(),
					AppConstants.NOTIFICATION_COLLECTION, NotificationModel.class);

		} catch (Exception ex) {
			logger.error(notification.getRrId(), AppConstants.RAVEN,
					"Failed to create documents for User IDs" + ex.getMessage(), null);

		}
		return result;
	}

	private Notification setUserId(Notification notification, String userId) {
		notification.setUserIds(Collections.singletonList(userId));
		return notification;
	}

    @Override
    @Async
	public void deleteChannelMessagesForUsers(ChannelUpdate channelUpdate, String clientId, String userId, String rrId, ChannelModel channelModel) {
			logger.info(rrId, AppConstants.RAVEN, "Deleting channel notification for users and userGroup", null);
			Set<String> userIds = new TreeSet<>();
			try {
			if (channelUpdate.getUserGroup() != null && !channelUpdate.getUserGroup().isEmpty()) {
				Set<String> groupsUserIds = getUserIdsForGroups(channelUpdate.getUserGroup(), clientId, rrId);
				if(groupsUserIds!= null && !groupsUserIds.isEmpty()) {
					groupsUserIds.removeAll(channelModel.getUsers());
					userIds.addAll(groupsUserIds);
				}
			}

			if (channelUpdate.getUsers()!=null &&!channelUpdate.getUsers().isEmpty())
				userIds.addAll(channelUpdate.getUsers());

	        if (!userIds.isEmpty()) {
	            Bson select = Filters.and(
	                    Filters.eq(AppConstants.CHANNEL_ID, channelUpdate.getChannelId()),
	                    Filters.in(AppConstants.USER_ID, userIds),
	                    Filters.ne(STATUS, DELETED)
	            );
	            Bson update = Updates.combine(
	                    set(STATUS, DELETED),
	                    set(AppConstants.UPDATED_AT, LocalDateTime.now(ZoneOffset.UTC)),
	                    set(AppConstants.UPDATED_BY, userId)
	            );
	            mongoRepository.updateMany(select, update, clientId, AppConstants.NOTIFICATION_COLLECTION, NotificationModel.class);
	        }
		}catch (Exception ex) {
			logger.error(rrId, AppConstants.RAVEN, "Failed to delete channel notification for  " + userIds + ex.getMessage(), null);
			new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
    }

	@Override
	@Async
	public void updateExpiredChannel(List<String> clientIdList, String rrId) {
		try {
			logger.info(rrId, AppConstants.RAVEN, "Deleting the channel notifications that are expired ", null);
			var expiredTimeStamp = LocalDateTime.now(ZoneOffset.UTC);
			for (String databaseName : clientIdList) {
				List<ObjectId> idsList = new ArrayList<>();
				try {
					logger.info(rrId, AppConstants.RAVEN, "Finding the channel notifications that are expired at " + expiredTimeStamp + " for " + databaseName , null);
					Bson filter = Filters.and(Filters.eq(AppConstants.MESSAGE_TYPE, AppConstants.CHANNEL_MSG),
							Filters.lt(AppConstants.EXPIRED_TIME, expiredTimeStamp),
							Filters.ne(AppConstants.STATUS, AppConstants.DELETED));
					List<NotificationModel> doc = mongoRepository.findAllNoLimit(filter, databaseName, AppConstants.NOTIFICATION_COLLECTION, NotificationModel.class);
					idsList = doc.stream().map(record -> record.getId()).collect(Collectors.toList());
					if (!idsList.isEmpty()) {
						logger.info(rrId, AppConstants.RAVEN, "Deleting " + idsList + " id's of client " + databaseName, null);

						Bson whereFilter = Filters.in(AppConstants._ID, idsList);
						Bson updateFilter = Updates.combine(Updates.set(AppConstants.STATUS, DELETED),
								Updates.set(AppConstants.UPDATED_AT, LocalDateTime.now(ZoneOffset.UTC)), Updates.set(AppConstants.UPDATED_BY, AppConstants.SCHEDULED_UPDATE));
						mongoRepository.updateMany(whereFilter, updateFilter, databaseName,
								AppConstants.NOTIFICATION_COLLECTION, NotificationModel.class);
						logger.info(rrId, AppConstants.RAVEN, "Successfully deleted" + idsList + " id's of client " + databaseName, null);
					}
					else{
						logger.error(rrId, AppConstants.RAVEN, "No expired channel notifications found for client " + databaseName, null);
					}

				} catch (Exception ex) {
					logger.error(rrId, AppConstants.RAVEN, "Failed to delete " + idsList + " of client " + databaseName + ex.getMessage(), null);
					new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
				}
			}
		} catch (Exception ex) {
			logger.error(rrId, AppConstants.RAVEN, "Failed to delete the expired channels " + ex.getMessage(), null);
			new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
	}
}
