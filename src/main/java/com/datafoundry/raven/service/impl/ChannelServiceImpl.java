package com.datafoundry.raven.service.impl;

import static com.datafoundry.raven.utils.AppConstants.ADD;
import static com.datafoundry.raven.utils.AppConstants.CHANNEL_COLLECTION;
import static com.datafoundry.raven.utils.AppConstants.CHANNEL_ID;
import static com.datafoundry.raven.utils.AppConstants.DELETE;
import static com.datafoundry.raven.utils.AppConstants.DELETED;
import static com.datafoundry.raven.utils.AppConstants.STATUS;
import static com.datafoundry.raven.utils.Utility.isListNotEmpty;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.ne;
import static com.mongodb.client.model.Updates.addEachToSet;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.pullAll;
import static com.mongodb.client.model.Updates.set;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.raven.entity.ChannelModel;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.models.Channel;
import com.datafoundry.raven.models.ChannelDelete;
import com.datafoundry.raven.models.ChannelUpdate;
import com.datafoundry.raven.models.Notification;
import com.datafoundry.raven.models.Pagination;
import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.models.UserSession;
import com.datafoundry.raven.repositories.MongoRepository;
import com.datafoundry.raven.service.AsyncNotificationService;
import com.datafoundry.raven.service.ChannelService;
import com.datafoundry.raven.utils.AppConstants;
import com.datafoundry.raven.utils.Utility;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

/**
 * @author ram
 * @version number Raven v1.0
 * @created 19/12/2020
 * @description description
 */

@Component
public class ChannelServiceImpl implements ChannelService {

	RRAdapter logger = new RRAdapterImpl(ChannelServiceImpl.class.getName());

	@Autowired
	MongoRepository<ChannelModel> mongoRepository;

    @Autowired
    AsyncNotificationService asyncNotificationService;

    @Override
    public boolean create(Channel channel, String rrId) {
        logger.info(rrId, AppConstants.RAVEN, "Create new channel", null);
        ChannelModel channelModel = toChannelModel(channel);
        ChannelModel result = mongoRepository.find(
                Filters.and(Filters.eq("channelName", channelModel.getChannelName()), Filters.ne(STATUS, DELETED)),
                channel.getClientId(), AppConstants.CHANNEL_COLLECTION, ChannelModel.class);
        if (result != null)
            throw new AppException(AppConstants.ALREADY_EXISTS, channelExistsMessage(channelModel.getChannelName()));
        else
            return mongoRepository.save(channelModel, channel.getClientId(), AppConstants.CHANNEL_COLLECTION,
                    ChannelModel.class);
    }

    @Override
    public Status delete(UserSession userSession, ChannelDelete channelDelete, String rrId) throws AppException {
        try {
            logger.info(rrId, AppConstants.RAVEN, "Delete existing channel", null);

            String channelIdValue = channelDelete.getChannelId();
            Bson whereQuery = Filters.and(Filters.eq(AppConstants.CHANNEL_ID, channelIdValue),
                    Filters.ne(AppConstants.STATUS, AppConstants.DELETED));
            Bson updateObject = Updates.combine(Updates.set(AppConstants.STATUS, AppConstants.DELETED),
                    Updates.set(AppConstants.UPDATED_AT, LocalDateTime.now(ZoneOffset.UTC)),
                    Updates.set(AppConstants.UPDATED_BY, userSession.getUserId()));
            var updatedResult = mongoRepository.updateOne(whereQuery, updateObject, userSession.getClientId(), AppConstants.CHANNEL_COLLECTION, ChannelModel.class);
            if (updatedResult == 0) {
                throw new AppException(AppConstants.CHANNEL_ID_NOT_FOUND, AppConstants.CHANNEL_ID_NOT_FOUND);
            }
            asyncNotificationService.updateAllNotificationStatusByChannelId(userSession, channelIdValue, AppConstants.DELETED, rrId);
            return new Status.StatusBuilder("Channel deleted successfully").isSuccess(true).withStatusCode(HttpStatus.OK.value()).build();
        } catch (AppException ae) {
            logger.error(rrId, AppConstants.RAVEN, Utility.getExceptionMessage(ae), null);
            throw new AppException(AppConstants.CHANNEL_ID_NOT_FOUND, AppConstants.CHANNEL_ID_NOT_FOUND);
        } catch (Exception ex) {
            logger.error(rrId, AppConstants.RAVEN, Utility.getExceptionMessage(ex), null);
            throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
        }

    }

    public long update(ChannelUpdate channelUpdate, String userId, String rrId) throws AppException {

        logger.info(rrId, AppConstants.RAVEN, "Update channel : " + channelUpdate.getChannelId(), null);
        ChannelModel result = mongoRepository.find(
                Filters.and(eq(CHANNEL_ID, channelUpdate.getChannelId()), Filters.ne(STATUS, DELETED)),
                channelUpdate.getClientId(),
                CHANNEL_COLLECTION,
                ChannelModel.class
        );
        if (result != null) {
            if ((channelUpdate.getDescription() != null && !channelUpdate.getDescription().isEmpty()) && (channelUpdate.getAction() == null || channelUpdate.getAction().isEmpty())) {
                return updateChannelDescription(channelUpdate, channelUpdate.getClientId(), userId);
            } else if (channelUpdate.getAction() != null && (channelUpdate.getUserGroup() != null || channelUpdate.getUsers() != null || channelUpdate.getDescription() != null )) {
                if (channelUpdate.getAction().equals(ADD)) {
                    return addUpdate(channelUpdate, channelUpdate.getClientId(), userId);
                } else if (channelUpdate.getAction().equals(DELETE)) {
                    return deleteUpdate(channelUpdate, channelUpdate.getClientId(), userId, rrId, result);
                } else {
                    throw new AppException(AppConstants.INVALID_DATA, AppConstants.NOTHING_TO_UPDATE);
                }

            } else {
                throw new AppException(AppConstants.NOTHING_TO_UPDATE, AppConstants.NOTHING_TO_UPDATE);
            }
        } else {
            throw new AppException(AppConstants.CHANNEL_ID_NOT_FOUND, AppConstants.CHANNEL_NOT_FOUND_TXT);
        }
    }

    public long updateChannelDescription(ChannelUpdate channelUpdate, String clientId, String userId) {
        Bson updateBson = combine(
                set(AppConstants.DESCRIPTION, channelUpdate.getDescription()),
                set(AppConstants.UPDATED_AT, LocalDateTime.now(ZoneOffset.UTC)),
                set(AppConstants.UPDATED_BY, userId)
        );
        return mongoRepository.updateOne(updateSelectFilter(clientId,channelUpdate.getChannelId()), updateBson, clientId, CHANNEL_COLLECTION, ChannelModel.class);
    }


    public long addUpdate(ChannelUpdate channelUpdate, String clientId, String userId) {
        List<Bson> list = new ArrayList<>();
        if(channelUpdate.getDescription()!=null)
            list.add(set(AppConstants.DESCRIPTION, channelUpdate.getDescription()));
        if (isListNotEmpty(channelUpdate.getUsers())) {
            list.add(addEachToSet(AppConstants.USERS, channelUpdate.getUsers().stream().distinct().collect(Collectors.toList())));
        }
        if (isListNotEmpty(channelUpdate.getUserGroup()))
            list.add(addEachToSet(AppConstants.USER_GROUP, channelUpdate.getUserGroup().stream().distinct().collect(Collectors.toList())));

        return executeUpdate(clientId, userId, channelUpdate.getChannelId(), list);
    }


    public long deleteUpdate(ChannelUpdate channelUpdate, String clientId, String userId, String rrId, ChannelModel channelModel) {

		List<Bson> list = new ArrayList<>();
		if (channelUpdate.getUsers() != null) {
			if (!channelModel.getUsers().containsAll(channelUpdate.getUsers())) {
				throw new AppException(AppConstants.NOTHING_TO_UPDATE, AppConstants.INVALID_USERS);
			}
		}
		if (channelUpdate.getUserGroup() != null) {
			if (!channelModel.getUserGroup().containsAll(channelUpdate.getUserGroup())) {
				throw new AppException(AppConstants.NOTHING_TO_UPDATE, AppConstants.INVALID_USERGROUP);
			}
		}
		
        if(channelUpdate.getDescription()!=null)
            list.add(set(AppConstants.DESCRIPTION, channelUpdate.getDescription()));
        if (isListNotEmpty(channelUpdate.getUsers())) {
            list.add(pullAll(AppConstants.USERS, channelUpdate.getUsers().stream().distinct().collect(Collectors.toList())));
        }
        if (isListNotEmpty(channelUpdate.getUserGroup()))
            list.add(pullAll(AppConstants.USER_GROUP, channelUpdate.getUserGroup().stream().distinct().collect(Collectors.toList())));

        long result = executeUpdate(clientId, userId, channelUpdate.getChannelId(), list);
        if (result > 0)
            asyncNotificationService.deleteChannelMessagesForUsers(channelUpdate, clientId, userId, rrId,channelModel);
        return result;
    }

    private long executeUpdate(String clientId, String userId,String channelId, List<Bson> list) {
        if (list.size() > 0) {
            list.add(set(AppConstants.UPDATED_AT, LocalDateTime.now(ZoneOffset.UTC)));
            list.add(set(AppConstants.UPDATED_BY, userId));
            return mongoRepository.updateOne(updateSelectFilter(clientId,channelId), combine(list), clientId, CHANNEL_COLLECTION, ChannelModel.class);
        } else {
            throw new AppException(AppConstants.NOTHING_TO_UPDATE, AppConstants.NOTHING_TO_UPDATE);
        }
    }

    private Bson updateSelectFilter(String clientId,String channelId){
        return Filters.and(eq(AppConstants.CLIENT_ID, clientId), eq(CHANNEL_ID, channelId), Filters.ne(STATUS, DELETED));
    }


    private ChannelModel toChannelModel(Channel channel) {
        return new ChannelModel(
                channel.getClientId(),
                channel.getUserGroup(),
                channel.getUsers(),
                channel.getChannelName(),
                channel.getDescription(),
                Utility.getChannelId(channel.getClientId()),
                AppConstants.ACTIVE,
                Utility.getUTCLocalDateTime(),
                channel.getRequestingService()
        );
    }

	private String channelExistsMessage(String channelName) {
		return String.format("Channel with name %s already exists", channelName);
	}

	@Override
	public ChannelModel getChannel(Notification notification) throws AppException {
		try {
			Bson filter = Filters.and(Filters.eq(AppConstants.CHANNEL_ID, notification.getChannelId()),
					Filters.nin(AppConstants.STATUS, AppConstants.CHANNEL_ACTIVE_STATUS_LIST));
			return mongoRepository.findOne(filter, notification.getClientId(), AppConstants.CHANNEL_COLLECTION,
					ChannelModel.class);
		} catch (Exception ex) {
			logger.error(notification.getRrId(), AppConstants.RAVEN, Utility.getExceptionMessage(ex), null);
			throw new AppException(AppConstants.CHANNEL_NOT_FOUND, AppConstants.CHANNEL_NOT_FOUND_TXT);
		}

	}


	@Override
	public List<ChannelModel> viewChannels(String clientId, Pagination request, String rrId)
			throws AppException {

		try {
			logger.info(rrId, AppConstants.RAVEN, "view channels", null);
			Utility.getPageNumber(request);
			Utility.getPageSize(request);

			Bson filter = getFilterByStatus(request.getStatus());
			Integer skip = Utility.findOffSet(request);

			return mongoRepository.filterDocuments(filter, skip, request.getPageSize(), clientId,
					AppConstants.CHANNEL_COLLECTION, ChannelModel.class);

		} catch (Exception ex) {
			logger.error(clientId, AppConstants.RAVEN, Utility.getExceptionMessage(ex), null);
			throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
	}

	private Bson getFilterByStatus(List<String> status) {
		if (status != null && !status.isEmpty()) {
			return Filters.in(AppConstants.STATUS, status);
		} else {
			return ne(AppConstants.STATUS, AppConstants.DELETED);
		}
	}
}
