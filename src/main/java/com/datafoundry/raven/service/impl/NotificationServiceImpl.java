package com.datafoundry.raven.service.impl;

import static com.mongodb.client.model.Filters.ne;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;

import com.datafoundry.raven.models.*;
import com.datafoundry.raven.utils.RequestType;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.raven.entity.ChannelModel;
import com.datafoundry.raven.entity.NotificationModel;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.repositories.MongoRepository;
import com.datafoundry.raven.service.AsyncNotificationService;
import com.datafoundry.raven.service.ChannelService;
import com.datafoundry.raven.service.NotificationService;
import com.datafoundry.raven.utils.AppConstants;
import com.datafoundry.raven.utils.Utility;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

/**
 * @author akhilesh, rama odedara
 * @created on 18/12/2020
 * @description Service implementation class for serving notification requests
 * @version number Raven v1.0
 */

@Component
public class NotificationServiceImpl implements NotificationService {

	RRAdapter logger = new RRAdapterImpl(NotificationServiceImpl.class.getName());

	@Autowired
	MongoRepository<NotificationModel> mongoRepository;

	@Autowired
	AsyncNotificationService asyncNotificationService;
	
	@Autowired
	ChannelService channelService;

	@Autowired
	RestService restService;

	@Value("${user.service.get.listClient.url}")
	String userServiceListClientUrl;

	@Override
	public void create(Notification notification) throws AppException {
		try {
			logger.info(notification.getRrId(), AppConstants.RAVEN, "Create new notification", null);

			if (notification.getMessageType().equals(AppConstants.CHANNEL_MSG)) {
				ChannelModel channel = channelService.getChannel(notification);
				if (channel == null){
					throw new AppException(AppConstants.CHANNEL_ID_NOT_FOUND, AppConstants.CHANNEL_NOT_FOUND_TXT);
				} else{
					asyncNotificationService.createChannelNotifications(notification, channel);
				}
			}
				notification.getUserIds().stream().forEach(userId -> {
					NotificationModel toWrite = Utility.createNotificationDBModel(notification, userId, null);
					mongoRepository.save(toWrite, toWrite.getClientId(), AppConstants.NOTIFICATION_COLLECTION,
							NotificationModel.class);
				});

		} catch (AppException ae) {
			logger.error(notification.getRrId(), AppConstants.RAVEN, Utility.getExceptionMessage(ae), null);
			throw ae;
		} catch (Exception ex) {
			logger.error(notification.getRrId(), AppConstants.RAVEN, Utility.getExceptionMessage(ex), null);
			throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
	}

	public List<NotificationModel> getUnDeliveredNotifications(UserSession userSession, String rrId,
			Pagination pagination) {
		try {
			logger.info(rrId, AppConstants.RAVEN, "Get All Undelivered Notifications", null);

			Utility.getPageNumber(pagination);
			Utility.getPageSize(pagination);
			Integer skip = Utility.findOffSet(pagination);

			Bson filter = Filters.and(Filters.eq(AppConstants.STATUS, AppConstants.UN_DELIVERED),
					Filters.eq(AppConstants.USER_ID, userSession.getUserId()));

			List<NotificationModel> notificationModelList = mongoRepository.findAll(filter, skip,
					pagination.getPageSize(), userSession.getClientId(), AppConstants.NOTIFICATION_COLLECTION,
					NotificationModel.class);

			if (notificationModelList != null && notificationModelList.size() > 0) {
				filter = Filters.and(Filters.in(AppConstants._ID,
						notificationModelList.stream().map(NotificationModel::getId).collect(Collectors.toList())));

				Bson update = Updates.combine(Updates.set(AppConstants.STATUS, AppConstants.DELIVERED),
						Updates.set(AppConstants.UPDATED_BY, userSession.getUserId()),
						Updates.set(AppConstants.UPDATED_AT, LocalDateTime.now(ZoneOffset.UTC)),
						Updates.set(AppConstants.DELIVERED_TIME, LocalDateTime.now(ZoneOffset.UTC)));
				asyncNotificationService.updateNotificationStatusByListOfIds(filter, update, userSession.getClientId(),
						AppConstants.NOTIFICATION_COLLECTION, NotificationModel.class, rrId);
			}
			return notificationModelList;

		} catch (Exception ex) {
			logger.error(rrId, AppConstants.RAVEN, Utility.getExceptionMessage(ex), null);
			throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
	}

	@Override
	public Status statusUpdate(UserSession userSession, NotificationUpdate notificationUpdate, String rrId)
			throws AppException {
		try {
			logger.info(rrId, AppConstants.RAVEN, "Updating the status for a given notificationID", null);
			String clientId = userSession.getClientId();
			String userId = userSession.getUserId();

			Bson whereQuery = Filters.and(Filters.eq(AppConstants.NOTIFICATION_ID, notificationUpdate.getNotificationId()),
					Filters.ne(AppConstants.STATUS, AppConstants.DELETED));
			Bson updateObject = Updates.combine(Updates.set(AppConstants.STATUS, notificationUpdate.getStatus()),
					Updates.set(AppConstants.UPDATED_AT, LocalDateTime.now(ZoneOffset.UTC)), Updates.set(AppConstants.UPDATED_BY, userId));

			var updatedResult = mongoRepository.updateOne(whereQuery, updateObject, clientId,
					AppConstants.NOTIFICATION_COLLECTION, NotificationModel.class);
			if (updatedResult == 0) {
				throw new AppException(AppConstants.NOTIFICATION_ID_NOT_FOUND, AppConstants.NOTIFICATION_ID_NOT_FOUND);
			}
			return new Status.StatusBuilder(AppConstants.UPDATED_ONLY_THE_STATUS_SUCCESSFULLY).isSuccess(true)
					.withStatusCode(HttpStatus.OK.value()).build();
		} catch (AppException ae) {
			logger.error(rrId, AppConstants.RAVEN, Utility.getExceptionMessage(ae), null);
			throw new AppException(AppConstants.NOTIFICATION_ID_NOT_FOUND, AppConstants.NOTIFICATION_ID_NOT_FOUND);
		} catch (Exception ex) {
			logger.error(rrId, AppConstants.RAVEN, Utility.getExceptionMessage(ex), null);
			throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
	}

	@Override
	public List<NotificationModel> readNotificationsByStatus(UserSession userSession, Pagination request, String rrId)
			throws AppException {
		try {
			logger.info(rrId, AppConstants.RAVEN, "read Notifications by status", null);
			Utility.getPageNumber(request);
			Utility.getPageSize(request);

			Bson filter = getFilterByStatus(request.getStatus(), userSession);
			Integer skip = Utility.findOffSet(request);

			List<NotificationModel> notificationModelList = mongoRepository.filterDocuments(filter, skip, request.getPageSize(), userSession.getClientId(),
					AppConstants.NOTIFICATION_COLLECTION, NotificationModel.class);
			
			if (notificationModelList != null && notificationModelList.size() > 0) {

				List<NotificationModel> listOfUndeliverNotifications = notificationModelList.stream()
						.filter(data -> data.getStatus().equals(AppConstants.UN_DELIVERED)).collect(Collectors.toList());

				filter = Filters.and(Filters.in(AppConstants._ID,
						listOfUndeliverNotifications.stream().map(NotificationModel::getId).collect(Collectors.toList())));

				Bson update = Updates.combine(Updates.set(AppConstants.STATUS, AppConstants.DELIVERED),
						Updates.set(AppConstants.UPDATED_BY, userSession.getUserId()),
						Updates.set(AppConstants.UPDATED_AT, LocalDateTime.now(ZoneOffset.UTC)),
						Updates.set(AppConstants.DELIVERED_TIME, LocalDateTime.now(ZoneOffset.UTC)));

				asyncNotificationService.updateNotificationStatusByListOfIds(filter, update, userSession.getClientId(),
						AppConstants.NOTIFICATION_COLLECTION, NotificationModel.class, rrId);
			}
			return notificationModelList;
		} catch (Exception ex) {
			logger.error(userSession.getClientId(), AppConstants.RAVEN, Utility.getExceptionMessage(ex), null);
			throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
 }

	private Bson getFilterByStatus(List<String> status, UserSession userSession) {
		Bson filter = null;
		if (status != null && !status.isEmpty()) {
			filter = Filters.and(Filters.in(AppConstants.STATUS, status),
					Filters.eq(AppConstants.USER_ID, userSession.getUserId()));
		} else {
			filter = Filters.and(ne(AppConstants.STATUS, AppConstants.DELETED),
					Filters.eq(AppConstants.USER_ID, userSession.getUserId()));
		}
		return filter;
	}

	@Override
	public void updateExpiredChannel(String rrId) {
		try {
			ResponseEntity<ListClientResponse> response = restService.getResponse(rrId,
					userServiceListClientUrl, RequestType.GET_CLIENTS, null, null, ListClientResponse.class);
			List<String> clientIdList = new ArrayList<>();
			response.getBody().getData().forEach(data -> clientIdList.add(data.getClientId()));
			if (!clientIdList.isEmpty()) {
				asyncNotificationService.updateExpiredChannel(clientIdList, rrId);
			} else {
				logger.info(rrId, AppConstants.RAVEN, AppConstants.NO_CLIENTS_FOUND_TO_UPDATE_EXPIRED_CHANNEL_NOTIFICATIONS, null);
			}
		} catch (Exception ex) {
			logger.info(rrId, AppConstants.RAVEN, "Failed to update expired channel notifications ", null);
			throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
	}
}
