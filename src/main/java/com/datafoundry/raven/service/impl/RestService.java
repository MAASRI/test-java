package com.datafoundry.raven.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.utils.AppConstants;
import com.datafoundry.raven.utils.RequestType;
import com.datafoundry.raven.utils.Utility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RestService {

	RRAdapter logger = new RRAdapterImpl(NotificationServiceImpl.class.getName());

	@Autowired
	ObjectMapper mapper;

	@Autowired
	RestTemplate restTemplate;

	public <T> ResponseEntity<T> getResponse(String rrId, String url, RequestType requestType, Object requestBody,
			Map<String, String> parameters, Class<T> returnType) {
		try {
			logger.info(rrId, AppConstants.RAVEN, "Calling external service:", null);
			HttpEntity<?> entity = getEntity(requestType, requestBody, parameters);
			ResponseEntity<T> response = restTemplate.exchange(url, getMethod(requestType), entity, returnType);
			return response;
		} catch (AppException ex) {
			logger.error(rrId, AppConstants.RAVEN, Utility.getExceptionMessage(ex), null);
			throw ex;
		} catch (HttpClientErrorException | HttpServerErrorException heex) {
			logger.error(rrId, AppConstants.RAVEN, heex.getStatusCode().value() + heex.getResponseBodyAsString(), null);
			throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		} catch (Exception ex) {
			logger.error(rrId, AppConstants.RAVEN, Utility.getExceptionMessage(ex), null);
			throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
	}

	private HttpEntity<?> getEntity(RequestType requestType, Object requestBody, Map<String, String> parameters) {

		HttpHeaders headers = createHeaders(requestType, parameters);
		HttpEntity<?> entity = null;
		try {
			switch (requestType) {
				case GET_AUTH_TOKEN:
				case GET_USER_IDS:
					entity = new HttpEntity<String>(mapper.writeValueAsString(requestBody), headers);
					break;
				case GET_CLIENTS:
					entity = new HttpEntity<String>(headers);
					break;
				default:
					throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
			}
		} catch (JsonProcessingException e) {
			throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
		return entity;
	}

	private HttpHeaders createHeaders(RequestType requestType, Map<String, String> parameters) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		switch (requestType) {
			case GET_USER_IDS:
			case GET_AUTH_TOKEN:
			case GET_CLIENTS:
				break;
			default:
				throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);
		}
		return headers;

	}

	private HttpMethod getMethod(RequestType requestType) {
		HttpMethod method = HttpMethod.GET;
		switch (requestType) {
			case GET_AUTH_TOKEN:
			case GET_USER_IDS:
			case GET_CLIENTS:
				method = HttpMethod.POST;
				break;
			default:
				throw new AppException(AppConstants.SERVICE_UNAVAILABLE, AppConstants.SOMETHING_WENT_WRONG);

		}
		return method;
	}
}
