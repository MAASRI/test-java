package com.datafoundry.raven.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.datafoundry.raven.entity.ChannelModel;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.models.Channel;
import com.datafoundry.raven.models.ChannelDelete;
import com.datafoundry.raven.models.ChannelUpdate;
import com.datafoundry.raven.models.Notification;
import com.datafoundry.raven.models.Pagination;
import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.models.UserSession;

/**
 * @author ram
 * @version number Raven v1.0
 * @created 19/12/2020
 * @description description
 */
@Service
public interface ChannelService {

    boolean create(Channel channel,String rrId);
    ChannelModel getChannel(Notification notification) throws AppException;
    long update(ChannelUpdate channelUpdate, String userId, String rrId) throws AppException;
    Status delete(UserSession userSession, ChannelDelete channelDelete, String rrId) throws AppException;
    List<ChannelModel> viewChannels(String clientId, Pagination request, String rrId)
			throws AppException ;


}
