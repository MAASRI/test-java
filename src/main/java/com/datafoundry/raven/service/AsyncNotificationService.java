package com.datafoundry.raven.service;

import java.util.List;
import java.util.Set;

import com.datafoundry.raven.models.ChannelUpdate;
import org.bson.conversions.Bson;

import com.datafoundry.raven.entity.ChannelModel;
import com.datafoundry.raven.entity.NotificationModel;
import com.datafoundry.raven.models.Notification;
import com.datafoundry.raven.models.UserSession;

import java.util.List;

public interface AsyncNotificationService {

	void updateNotificationStatusByListOfIds(Bson filter, Bson update, String clientId, String notificationCollection,
			Class<NotificationModel> typeOfClass, String rrId);

	void createChannelNotifications(Notification notification, ChannelModel channel) throws InterruptedException;
	void deleteChannelMessagesForUsers(ChannelUpdate channelUpdate, String clientId, String userId, String rrId, ChannelModel channelModel);

	void updateAllNotificationStatusByChannelId(UserSession userSession, String channelId, String status, String rrId);
	void updateExpiredChannel(List<String> clientIdList, String rrId);

	Boolean saveNotificationsForUserIds(Notification notification, Set<String> userIds);

	Set<String> getUserGroupUserIds(List<String> groupIds, Notification notification);

}
