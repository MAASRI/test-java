package com.datafoundry.raven.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.datafoundry.raven.entity.NotificationModel;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.models.Notification;
import com.datafoundry.raven.models.NotificationUpdate;
import com.datafoundry.raven.models.Pagination;
import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.models.UserSession;

/**
 * @author akhilesh, rama odedara
 * @created on 18/12/2020
 * @description Service abstraction for serving notification requests
 * @version number Raven v1.0
 */

@Service
public interface NotificationService {
    void create(Notification notification) throws AppException;
	List<NotificationModel> getUnDeliveredNotifications(UserSession userSession, String rrId,  Pagination pagination);
    Status statusUpdate(UserSession userSession, NotificationUpdate notificationUpdate, String rrId);
    List<NotificationModel> readNotificationsByStatus(UserSession userSession, Pagination request, String rrId) throws AppException;
    void updateExpiredChannel(String rrId);
}
