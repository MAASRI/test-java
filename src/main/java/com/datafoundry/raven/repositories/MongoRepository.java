package com.datafoundry.raven.repositories;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.datafoundry.raven.utils.AppConstants;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;

/**
 * @author akhilesh, rama odedara
 * @version number Raven v1.0
 * @created on 18/12/2020
 * @description Class containing CRUD operation methods to communicate with DB
 */

@Repository
public class MongoRepository<T> {

    @Value("${raven.default.db}")
    private String defaultDB;

    @Autowired
    MongoClient mongoClient;

	public MongoCollection<T> getCollection(String databaseName, String collection, Class<T> type) {
		return mongoClient.getDatabase(getDatabaseName(databaseName)).getCollection(collection, type);
	}

	public boolean save(T obj, String databaseName, String collection, Class<T> type) {
		return getCollection(databaseName, collection, type).insertOne(obj).wasAcknowledged();
	}
	
	public boolean saveMany(List<T> obj, String databaseName, String collection, Class<T> type) {
		return getCollection(databaseName, collection, type).insertMany(obj).wasAcknowledged();
	}
	
	public T findOne(Bson filter, String databaseName, String collection, Class<T> type) {
		return getCollection(databaseName, collection, type).find(filter).first();
	}

    public void updateMany(Bson filter, Bson update, String databaseName, String collection, Class<T> type) {
        getCollection(databaseName, collection, type).updateMany(filter, update);
    }

    public List<T> findAll(Bson filter, Integer skip, Integer limit, String databaseName, String collection, Class<T> type) {
        return getCollection(databaseName, collection, type).find(filter).skip(skip).sort(new Document(AppConstants._ID, 1))
                .limit(limit).into(new ArrayList<>());
    }

    public Long updateOne(Bson whereQuery, Bson updateObject, String databaseName, String collection, Class<T> type) {
        return (getCollection(databaseName, collection, type).updateOne(whereQuery, updateObject).getModifiedCount());
    }


    public T find(Bson filter, String databaseName, String collection, Class<T> type) {
        return (getCollection(databaseName, collection, type).find(filter).first());
    }

	public List<T> filterDocuments(Bson filter, int skip, int limit, String databaseName, String collection,
			Class<T> type) {
		return getCollection(databaseName, collection, type).find(filter).skip(skip).sort(new Document("_id", 1))
				.limit(limit).into(new ArrayList<>());
	}

	public String getDatabaseName(String databaseName) {
		if (databaseName == null || databaseName.trim().isEmpty())
			return defaultDB;
		else
			return databaseName.trim();
	}

	public List<T> findAllNoLimit(Bson filter, String databaseName, String collection, Class<T> type) {
		return getCollection(databaseName, collection, type).find(filter).into(new ArrayList<>());
	}
}
