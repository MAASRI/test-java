package com.datafoundry.raven.exception;

import java.io.Serializable;
/**
 * @author akhilesh
 * @created on 18/12/2020
 * @description POJO 
 * @version number Raven v1.0
 */
public class ErrorMessage implements Serializable{

	
	private static final long serialVersionUID = 116565L;

	private String errorCode;
	
	private String errorMessage;

	public ErrorMessage() {
		super();
	}

	public ErrorMessage(String errorCode, String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
}
