package com.datafoundry.raven.exception;

/**
 * @author akhilesh
 * @created on 18/12/2020
 * @description POJO for custom app exception
 * @version number Raven v1.0
 */

public class AppException extends RuntimeException {

	private static final long serialVersionUID = 123234L;

	private String errorCode;

	private String errorMessage;

	public AppException(String errorCode, String errorMessage) {
		super(errorCode);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public AppException() {
		super("");
	}

	public AppException(String errorCode, Throwable cause) {
		super(errorCode, cause);
		this.errorCode = errorCode;
		this.errorMessage = cause.getMessage();

	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
