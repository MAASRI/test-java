package com.datafoundry.raven.exception;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.models.Status.StatusBuilder;
import com.datafoundry.raven.utils.AppConstants;

/**
 * @author akhilesh
 * @created on 18/12/2020
 * @description class for handling responses for multiple exceptions
 * @version number Raven v1.0
 */
@Configuration
@ControllerAdvice(annotations = RestController.class)
public class AppExceptionHandler {

	@ExceptionHandler(value = {HttpMessageNotReadableException.class})
	public ResponseEntity<Status> handleHttpMessageNotReadable(HttpMessageNotReadableException ex) {
		return new ResponseEntity<Status>(new Status.StatusBuilder(ex.getMessage()).isSuccess(false)
				.withStatusCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = { AppException.class })
	public ResponseEntity<Status> handleCustomException(AppException ex) {

		StatusBuilder statusBuilder = new Status.StatusBuilder(ex.getErrorMessage()).isSuccess(false);
		if (ex.getErrorCode().equals(AppConstants.INVALID_DATA))
			return new ResponseEntity<Status>(statusBuilder.withStatusCode(HttpStatus.BAD_REQUEST.value()).build(),
					HttpStatus.BAD_REQUEST);
		else if(ex.getErrorCode().equals(AppConstants.NO_DATA))
			return new ResponseEntity<Status>(statusBuilder.withStatusCode(HttpStatus.NO_CONTENT.value()).build(),
					HttpStatus.NO_CONTENT);
		if (ex.getErrorCode().equals(AppConstants.NOTIFICATION_ID_NOT_FOUND) || ex.getErrorCode().equals(AppConstants.CHANNEL_ID_NOT_FOUND))
			return new ResponseEntity<Status>(statusBuilder.withStatusCode(HttpStatus.NOT_FOUND.value()).build(),
					HttpStatus.NOT_FOUND);
		if(ex.getErrorCode().equals(AppConstants.NO_DATA))
			return new ResponseEntity<Status>(statusBuilder.withStatusCode(HttpStatus.NO_CONTENT.value()).build(),
					HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<Status>(
					statusBuilder.withStatusCode(HttpStatus.UNPROCESSABLE_ENTITY.value()).build(),
					HttpStatus.UNPROCESSABLE_ENTITY);

	}

}
