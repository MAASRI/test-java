package com.datafoundry.raven.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.raven.service.impl.NotificationServiceImpl;
import com.datafoundry.raven.utils.AppConstants;
import com.datafoundry.raven.utils.Utility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author akhilesh
 * @created on 18/12/2020
 * @description Class for logging outgoing response payload
 * @version number Raven v1.0
 */

@ControllerAdvice
public class ApplicationResponseAdviceAdapter implements ResponseBodyAdvice<Object> {

	RRAdapter logger = new RRAdapterImpl(NotificationServiceImpl.class.getName());

	@Value("${app.rr-id}")
	private String appRRId;

	@Autowired
	protected ObjectMapper mapper;

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {

		return true;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		try {
			if (!request.getURI().toString().contains(AppConstants.API_DOCS_PATH))
				logger.debug(appRRId, AppConstants.RAVEN, "Response Body: " + mapper.writeValueAsString(body), null);
		} catch (JsonProcessingException e) {
			logger.error(appRRId, AppConstants.RAVEN, "ERROR READING RESPONSE : " + Utility.getExceptionMessage(e),
					null);
		}

		return body;
	}

}
