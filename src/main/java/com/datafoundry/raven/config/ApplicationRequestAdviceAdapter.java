package com.datafoundry.raven.config;

import java.lang.reflect.Type;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.raven.service.impl.NotificationServiceImpl;
import com.datafoundry.raven.utils.AppConstants;
import com.datafoundry.raven.utils.Utility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author akhilesh
 * @created on 18/12/2020
 * @description Class for logging incoming request payload
 * @version number Raven v1.0
 */

@ControllerAdvice
public class ApplicationRequestAdviceAdapter extends RequestBodyAdviceAdapter {

	RRAdapter logger = new RRAdapterImpl(NotificationServiceImpl.class.getName());

	@Value("${app.rr-id}")
	private String appRRId;

	@Autowired
	protected ObjectMapper mapper;

	@Override
	public boolean supports(MethodParameter methodParameter, Type type,
			Class<? extends HttpMessageConverter<?>> aClass) {
		return true;
	}

	@Override
	public Object afterBodyRead(Object body, HttpInputMessage httpInputMessage, MethodParameter methodParameter,
			Type type, Class<? extends HttpMessageConverter<?>> aClass) {
		try {
			logger.debug(appRRId, AppConstants.RAVEN, "Request Body: " + mapper.writeValueAsString(body), null);
		} catch (JsonProcessingException e) {
			logger.error(appRRId, AppConstants.RAVEN, "ERROR READING RESPONSE : " + Utility.getExceptionMessage(e),
					null);
		}
		return body;
	}
}