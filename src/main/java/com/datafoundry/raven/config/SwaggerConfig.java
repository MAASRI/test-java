package com.datafoundry.raven.config;

import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author akhilesh
 * @version number Raven v1.0
 * @created on 18/12/2020
 * @description Class for Swagger UI
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${raven.base-url}")
    private String ravenBaseURL;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).host(ravenBaseURL.replaceFirst("^(http[s]?://)", "")).select().apis(RequestHandlerSelectors.any())
                .paths(Predicate.not(PathSelectors.regex("/error"))).build().apiInfo(metadata())
                .useDefaultResponseMessages(false);

    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder().title("Raven Service API")
                .description("This is an application for handling notifications").version("1.0.0").build();
    }
}
