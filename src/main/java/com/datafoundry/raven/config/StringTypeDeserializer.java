package com.datafoundry.raven.config;

import java.io.IOException;

import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.utils.AppConstants;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class StringTypeDeserializer extends JsonDeserializer<String> {
	@Override
	public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		JsonToken t = p.getCurrentToken();
		if (t.isBoolean()) {
			throw new AppException(AppConstants.INVALID_DATA_TEXT, AppConstants.INVALID_DATA_TEXT);
		} else if (t.isNumeric()) {
			throw new AppException(AppConstants.INVALID_DATA_TEXT, AppConstants.INVALID_DATA_TEXT);
		} else if (t.equals(JsonToken.VALUE_STRING)) {
			return p.getValueAsString();
		}
		return null;
	}
}
