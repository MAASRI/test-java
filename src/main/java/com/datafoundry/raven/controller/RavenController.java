package com.datafoundry.raven.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.utils.AppConstants;

/**
 * @author akhilesh
 * @created on 18/12/2020
 * @description Class for checking service status
 * @version number Raven v1.0
 */

@RestController
public class RavenController {

	@RequestMapping(value = AppConstants.STATUS_PATH, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getAppStatus() {
		Status status = new Status.StatusBuilder(AppConstants.APPLICATION_RUNNING).isSuccess(true)
				.withStatusCode(HttpStatus.OK.value()).build();
		return new ResponseEntity<>(status, HttpStatus.OK);
	}

}
