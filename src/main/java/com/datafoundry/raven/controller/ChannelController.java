package com.datafoundry.raven.controller;

import static com.datafoundry.raven.utils.AppConstants.HEADER;
import static com.datafoundry.raven.utils.AppConstants.INVALID_DATA;
import static com.datafoundry.raven.utils.AppConstants.RR_ID;
import static com.datafoundry.raven.utils.AppConstants.USER_SESSION;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.datafoundry.raven.entity.ChannelModel;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.models.Channel;
import com.datafoundry.raven.models.ChannelDelete;
import com.datafoundry.raven.models.ChannelUpdate;
import com.datafoundry.raven.models.Pagination;
import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.models.UserSession;
import com.datafoundry.raven.service.ChannelService;
import com.datafoundry.raven.utils.AppConstants;
import com.datafoundry.raven.utils.SwaggerConstants;
import com.datafoundry.raven.utils.Utility;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import springfox.documentation.annotations.ApiIgnore;



/**
 * @author ram
 * @version number Raven v1.0
 * @created 19/12/2020
 * @description description
 */
@RestController
@RequestMapping(AppConstants.CHANNEL_PATH)
public class ChannelController {
	@Value("${raven.default.db}")
	private String defaultDB;

	@Autowired
	ChannelService channelService;


	@ApiOperation(value = SwaggerConstants.CREATE_CHANNEL_TITLE, response = Status.class)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = AppConstants.CREATED_SUCCESSFULLY, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.CREATE_SUCCESS_RESPONSE, mediaType = "*/*"))),
			@ApiResponse(code = 400, message = SwaggerConstants.BAD_REQUEST, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.POST_400_RESPONSE, mediaType = "*/*"))),
			@ApiResponse(code = 422, message = SwaggerConstants.UNPROCESSABLE_ENTITY, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.POST_422_RESPONSE, mediaType = "*/*"))), })
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name =USER_SESSION, value =USER_SESSION, paramType = AppConstants.HEADER, dataTypeClass = UserSession.class, example = SwaggerConstants.USER_SESSION_EXAMPLE),
			@ApiImplicitParam(name = AppConstants.RR_ID, value = AppConstants.RR_ID, paramType = AppConstants.HEADER, example = SwaggerConstants.RR_ID_EXAMPLE) })
	@RequestMapping(value = AppConstants.CREATE_PATH, method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Status create(
			@RequestHeader(name = AppConstants.RR_ID, required = false) String rrIdHeader,
			@ApiIgnore @RequestHeader(name =USER_SESSION, required = false) UserSession userSession,
			@RequestBody @Valid Channel channel,
			BindingResult bindingResult
	) throws AppException {
		String rrId = Utility.getRRId(rrIdHeader);
		if (bindingResult.hasErrors()) {
			throw new AppException(AppConstants.INVALID_DATA, Utility.getFirstErrorInformation(bindingResult, rrId));
		}
		Utility.validateCreateChannelModel(channel, rrId);
		channel.setClientId(Utility.getClientId(channel.getClientId(),userSession,defaultDB));
		return Utility.insertResponse(channelService.create(channel, rrId));
	}
	@ApiOperation(value = SwaggerConstants.UPDATE_CHANNEL_TITLE, response = Status.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = AppConstants.UPDATED_SUCCESSFULLY, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.UPDATE_SUCCESS_RESPONSE, mediaType = "*/*"))),
			@ApiResponse(code = 400, message = SwaggerConstants.BAD_REQUEST, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.UPDATE_CHANNEL_400_RESPONSE, mediaType = "*/*"))),
			@ApiResponse(code = 422, message = SwaggerConstants.UNPROCESSABLE_ENTITY, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.UPDATE_CHANNEL_422_RESPONSE, mediaType = "*/*"))), })

	@ApiImplicitParams(value = {
			@ApiImplicitParam(name =USER_SESSION, value =USER_SESSION, paramType = AppConstants.HEADER, dataTypeClass = UserSession.class, example = SwaggerConstants.USER_SESSION_EXAMPLE),
			@ApiImplicitParam(name = AppConstants.RR_ID, value = AppConstants.RR_ID, paramType = AppConstants.HEADER, example = SwaggerConstants.RR_ID_EXAMPLE) })
    @RequestMapping(value = AppConstants.UPDATE_PATH, method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public Status update(
            @RequestHeader(name = RR_ID, required = false) String rrId,
            @ApiIgnore @RequestHeader(name = USER_SESSION, required = false) UserSession userSession,
            @RequestBody @Valid ChannelUpdate channelUpdate,
			BindingResult bindingResult
	) throws AppException {
        String validRRId = Utility.getRRId(rrId);

        if (bindingResult.hasErrors()) {
            throw new AppException(INVALID_DATA, Utility.getFirstErrorInformation(bindingResult, validRRId));
        }
        Utility.validateUserSession(userSession);
        Utility.validateChannelUpdate(channelUpdate);
        channelUpdate.setClientId(Utility.getClientId(channelUpdate.getClientId(),userSession,defaultDB));
        channelService.update(channelUpdate, userSession.getUserId(),validRRId);
        return Utility.UPDATE_RESPONSE;
    }

    @ApiOperation(value = SwaggerConstants.DELETE_CHANNEL_TITLE, response = Status.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = AppConstants.DELETED_SUCCESSFULLY, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.DELETE_SUCCESS_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 400, message = SwaggerConstants.BAD_REQUEST, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.DELETE_400_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 404, message = SwaggerConstants.NOT_FOUND, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.DELETE_404_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 422, message = SwaggerConstants.UNPROCESSABLE_ENTITY, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.DELETE_422_RESPONSE, mediaType = "*/*")))
    })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name =USER_SESSION, value =USER_SESSION, paramType = AppConstants.HEADER, dataTypeClass = UserSession.class, example = SwaggerConstants.USER_SESSION_EXAMPLE),
            @ApiImplicitParam(name = AppConstants.RR_ID, value = AppConstants.RR_ID, paramType = AppConstants.HEADER, example = SwaggerConstants.RR_ID_EXAMPLE)
    })
    @RequestMapping(value = AppConstants.DELETE_PATH, method = RequestMethod.PUT)
    public Status deleteChannel(
    		@RequestHeader(name = AppConstants.RR_ID, required = false) String rrId,
			@ApiIgnore @RequestHeader(name =USER_SESSION, required = false) UserSession userSession,
			@RequestBody @Valid ChannelDelete channelDelete,
			BindingResult bindingResult
	) throws AppException {
        String validRRId = Utility.getRRId(rrId);
        if (bindingResult.hasErrors()) {
            throw new AppException(AppConstants.INVALID_DATA, Utility.getFirstErrorInformation(bindingResult, validRRId));
        }
        Utility.validateUserSession(userSession);
        Utility.validateChannelDeleteStatus(channelDelete);
        userSession.setClientId(Utility.getClientId(channelDelete.getClientId(),userSession,defaultDB));
       return channelService.delete(userSession, channelDelete, rrId);

    }

    @ApiOperation(value = SwaggerConstants.VIEW_CHANNEL_TITLE, response = Status.class)
    @ApiResponses(value = {
			@ApiResponse(code = 200, message = SwaggerConstants.SUCCESS, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.VIEW_CHANNEL_SUCCESS_RESPONSE, mediaType = "*/*"))),
			@ApiResponse(code = 204, message = SwaggerConstants.NO_CONTENT, response = Status.class, examples = @Example(value = @ExampleProperty(value = "", mediaType = "*/*"))),
			@ApiResponse(code = 400, message = SwaggerConstants.BAD_REQUEST, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.CHANNEL_400_RESPONSE, mediaType = "*/*"))),
			@ApiResponse(code = 422, message = SwaggerConstants.UNPROCESSABLE_ENTITY, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.CHANNEL_422_RESPONSE, mediaType = "*/*"))), })
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = AppConstants.USER_SESSION, value = AppConstants.USER_SESSION, paramType = AppConstants.HEADER, dataTypeClass = UserSession.class, example = SwaggerConstants.USER_SESSION_EXAMPLE),
			@ApiImplicitParam(name = RR_ID, value = RR_ID,  paramType = HEADER, example = SwaggerConstants.RR_ID_EXAMPLE) })
	@RequestMapping(value = AppConstants.VIEW_PATH, method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<ChannelModel>> viewChannel(
			@ApiIgnore @RequestHeader(name = USER_SESSION,required = false) UserSession userSession,
			@RequestHeader(name = RR_ID, required = false) String rrIdHeader,
			@RequestBody(required = false) @Valid Pagination request,
			BindingResult bindingResult
	) throws AppException {

		String rrId = Utility.getRRId(rrIdHeader);
		if(request == null)
			   request = new Pagination();
		
		if (bindingResult.hasErrors()) {
			throw new AppException(AppConstants.INVALID_DATA, Utility.getFirstErrorInformation(bindingResult, rrId));
		}

		Utility.validateUserSession(userSession);
		Utility.validateStatus(request, AppConstants.CHANNEL_STATUS_LIST);
		List<ChannelModel> result = channelService.viewChannels(Utility.getClientId(request.getClientId(),userSession,defaultDB), request, rrId);
		if (result == null || result.size() == 0)
			throw new AppException(AppConstants.NO_DATA, AppConstants.NO_DATA_FOUND);

		return new ResponseEntity<>(result, HttpStatus.OK);

	}
}