package com.datafoundry.raven.controller;

import static com.datafoundry.raven.utils.AppConstants.HEADER;
import static com.datafoundry.raven.utils.AppConstants.RR_ID;
import static com.datafoundry.raven.utils.AppConstants.USER_SESSION;
import static com.datafoundry.raven.utils.SwaggerConstants.RR_ID_EXAMPLE;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.datafoundry.raven.entity.NotificationModel;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.models.Notification;
import com.datafoundry.raven.models.NotificationUpdate;
import com.datafoundry.raven.models.Pagination;
import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.models.UserSession;
import com.datafoundry.raven.service.NotificationService;
import com.datafoundry.raven.utils.AppConstants;
import com.datafoundry.raven.utils.SwaggerConstants;
import com.datafoundry.raven.utils.Utility;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author rama odedara
 * @version number Raven v1.0
 * @created on 18/12/2020
 * @description Class for notifications incoming requests
 */
@RestController
@RequestMapping(AppConstants.NOTIFICATION_PATH)
public class NotificationController {
	@Value("${raven.default.db}")
	private String defaultDB;

	@Autowired
	NotificationService notificationService;

	@ApiOperation(value = SwaggerConstants.CREATE_NOTIFICATION_TITLE, response = NotificationModel.class)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = SwaggerConstants.NOTIFICATION_CREATED_SUCCESS_MESSAGE, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.CREATE_NOTIFICATION_SUCCESS_RESPONSE, mediaType = "*/*"))),
			@ApiResponse(code = 400, message = SwaggerConstants.BAD_REQUEST, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.NOTIFICATION_POST_400_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 404, message = SwaggerConstants.NOT_FOUND, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.NOTIFICATION_Post_404_RESPONSE, mediaType = "*/*"))),
			@ApiResponse(code = 422, message = SwaggerConstants.UNPROCESSABLE_ENTITY, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.NOTIFICATION_POST_422_RESPONSE, mediaType = "*/*"))) })
	@ApiImplicitParams(value = {
            @ApiImplicitParam(name = USER_SESSION, value = USER_SESSION, paramType = HEADER, dataTypeClass = UserSession.class, example = SwaggerConstants.USER_SESSION_EXAMPLE),
            @ApiImplicitParam(name = RR_ID, value = RR_ID, paramType = HEADER, example = RR_ID_EXAMPLE)
    })
	@PostMapping(AppConstants.CREATE_PATH)
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseEntity<Status> createNotification(
			@RequestHeader(name = RR_ID, required = false) String rrIdHeader,
			@ApiIgnore @RequestHeader(value = AppConstants.USER_SESSION, required = false) UserSession userSession,
			@RequestBody @Valid Notification notification,
			BindingResult bindingResult
	) throws AppException {
		notification.setRrId(Utility.getRRId(rrIdHeader));
		if (bindingResult.hasErrors()) {
			throw new AppException(AppConstants.INVALID_DATA,
					Utility.getFirstErrorInformation(bindingResult, notification.getRrId()));
		}
		Utility.validateCreateNotificationModel(notification, userSession);
		notification.setClientId(Utility.getClientId(notification.getClientId(),userSession,defaultDB));
		notificationService.create(notification);
		Status status = new Status.StatusBuilder(AppConstants.CREATED_SUCCESSFULLY)
				.isSuccess(true)
				.withStatusCode(HttpStatus.CREATED.value())
				.build();
		return new ResponseEntity<>(status, HttpStatus.CREATED);

	}

    @ApiOperation(value = SwaggerConstants.GET_UNDELIVERED_NOTIFICATIONS_TITLE)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = SwaggerConstants.SUCCESS, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.UNDELIVERED_NOTIFICATION_SUCCESS_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 400, message = SwaggerConstants.BAD_REQUEST,response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.NOTIFICATION_GET_400_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 204, message = SwaggerConstants.NO_CONTENT, response = Status.class, examples = @Example(value = @ExampleProperty(value = "", mediaType = "*/*"))),
            @ApiResponse(code = 422, message = SwaggerConstants.UNPROCESSABLE_ENTITY, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.NOTIFICATION_POST_422_RESPONSE, mediaType = "*/*")))})
    @ApiImplicitParams(value = {
			@ApiImplicitParam(name = AppConstants.USER_SESSION, required = false, value = AppConstants.USER_SESSION, paramType = AppConstants.HEADER, dataTypeClass = UserSession.class, example = SwaggerConstants.USER_SESSION_EXAMPLE),
			@ApiImplicitParam(name = RR_ID, value = RR_ID, required = false,  paramType = HEADER, example = RR_ID_EXAMPLE) })
    @PostMapping(AppConstants.GET_NOTIFICATION)

    public ResponseEntity<List<NotificationModel>> getUnDeliveredNotifications(
			@RequestHeader(value = AppConstants.RR_ID, required = false) String rrId,
			@ApiIgnore  @RequestHeader(value = AppConstants.USER_SESSION, required = false) UserSession userSession,
			@RequestBody(required = false) @Valid Pagination pagination,
			BindingResult bindingResult
			) throws AppException {
		rrId = Utility.getRRId(rrId);
		if (pagination == null)
			pagination = new Pagination();
        if (bindingResult.hasErrors()) {
            throw new AppException(AppConstants.INVALID_DATA, Utility.getFirstErrorInformation(bindingResult, rrId));
        }
        Utility.validateUUIDs(List.of(rrId));
        Utility.validateUserSession(userSession);
        userSession.setClientId(Utility.getClientId(pagination.getClientId(),userSession,defaultDB));
        List<NotificationModel> notificationList = notificationService.getUnDeliveredNotifications(userSession, rrId, pagination);
        if (notificationList == null || notificationList.size() == 0) 
        	throw new AppException(AppConstants.NO_DATA, AppConstants.NO_DATA_FOUND);
        return new ResponseEntity<>(notificationList, HttpStatus.OK);

	}

    @ApiOperation(value = SwaggerConstants.UPDATE_NOTIFICATION_TITLE, response = Status.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = SwaggerConstants.NOTIFICATION_UPDATED_SUCCESS_MESSAGE, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.UPDATE_NOTIFICATION_SUCCESS_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 400, message = SwaggerConstants.BAD_REQUEST, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.NOTIFICATION_PUT_400_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 404, message = SwaggerConstants.NOT_FOUND, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.NOTIFICATION_PUT_404_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 422, message = SwaggerConstants.UNPROCESSABLE_ENTITY, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.NOTIFICATION_PUT_422_RESPONSE, mediaType = "*/*"))),})
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = USER_SESSION, value = USER_SESSION, paramType = HEADER, dataTypeClass = UserSession.class, format = MediaType.APPLICATION_JSON_VALUE, example = SwaggerConstants.USER_SESSION_EXAMPLE),
            @ApiImplicitParam(name = RR_ID, value = RR_ID,  paramType = HEADER, example = RR_ID_EXAMPLE)
    })
    @PutMapping(AppConstants.UPDATE_PATH)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<Status> updateNotification(
            @RequestHeader(name = RR_ID, required = false) String rrIdHeader,
            @ApiIgnore @RequestHeader(name = USER_SESSION,required = false) UserSession userSession,
            @RequestBody @Valid NotificationUpdate notificationUpdate,
			BindingResult bindingResult) {
        String rrId = Utility.getRRId(rrIdHeader);
        if (bindingResult.hasErrors()) {
            throw new AppException(AppConstants.INVALID_DATA, Utility.getFirstErrorInformation(bindingResult, rrId));
        }
        Utility.validateNotificationUpdateModelStatus(notificationUpdate);
		Utility.validateUserSession(userSession);
		userSession.setClientId(Utility.getClientId(notificationUpdate.getClientId(),userSession,defaultDB));
        Status result = notificationService.statusUpdate(userSession, notificationUpdate, rrId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

	@ApiOperation(value = SwaggerConstants.READ_NOTIFICATION_BYSTATUS_TITLE, response = Status.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = SwaggerConstants.SUCCESS, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.READ_NOTIFICATION_SUCCESS_RESPONSE, mediaType = "*/*"))),
			@ApiResponse(code = 204, message = SwaggerConstants.NO_CONTENT, response = Status.class, examples = @Example(value = @ExampleProperty(value = "", mediaType = "*/*"))),
			@ApiResponse(code = 400, message = SwaggerConstants.BAD_REQUEST, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.NOTIFICATION_GET_400_RESPONSE, mediaType = "*/*"))),
			@ApiResponse(code = 422, message = SwaggerConstants.UNPROCESSABLE_ENTITY, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.RESPONSE_422, mediaType = "*/*"))), })
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = AppConstants.USER_SESSION, value = AppConstants.USER_SESSION, paramType = AppConstants.HEADER, dataTypeClass = UserSession.class, example = SwaggerConstants.USER_SESSION_EXAMPLE),
			@ApiImplicitParam(name = RR_ID, value = RR_ID,  paramType = HEADER, example = RR_ID_EXAMPLE) })
	@RequestMapping(value = AppConstants.READ_PATH, method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<NotificationModel>> getNotifications(
			@RequestHeader(name = RR_ID, required = false) String rrIdHeader,
			@ApiIgnore @RequestHeader(name = AppConstants.USER_SESSION, required = false) UserSession userSession,
			@RequestBody(required = false) @Valid Pagination request,
			BindingResult bindingResult
	) throws AppException {
		String rrId = Utility.getRRId(rrIdHeader);
		
		if (request == null)
			request = new Pagination();
		
		if (bindingResult.hasErrors()) {
			throw new AppException(AppConstants.INVALID_DATA, Utility.getFirstErrorInformation(bindingResult, rrId));
		}

		Utility.validateUserSession(userSession);
		Utility.validateStatus(request, AppConstants.NOTIFICATION_STATUS_LIST);
		userSession.setClientId(Utility.getClientId(request.getClientId(),userSession,defaultDB));
		List<NotificationModel> result = notificationService.readNotificationsByStatus(userSession, request, rrId);
		if (result == null || result.size() == 0)
			throw new AppException(AppConstants.NO_DATA, AppConstants.NO_DATA_FOUND);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@ApiOperation(value = SwaggerConstants.UPDATE_EXPIRED_NOTIFICATION_TITLE, response = Status.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = SwaggerConstants.SUCCESS, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.UPDATE_EXPIRED_NOTIFICATION_SUCCESS_RESPONSE, mediaType = "*/*"))),})
	@RequestMapping(value = AppConstants.UPDATE_EXPIRED, method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Status updateExpiredChannelNotifications(
			@RequestHeader(name = AppConstants.RR_ID, required = false) String rrIdHeader
	) {
		String rrId = Utility.getRRId(rrIdHeader);
		notificationService.updateExpiredChannel(rrId);
		return new Status.StatusBuilder("Request received!").isSuccess(true).withStatusCode(HttpStatus.OK.value()).build();
	}
}
