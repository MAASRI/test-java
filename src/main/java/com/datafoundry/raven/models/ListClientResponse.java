package com.datafoundry.raven.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.List;

public class ListClientResponse implements Serializable {

    private static final long serialVersionUID = 14668534L;

    public String statusCode;
    public String clientCount;
    @JsonDeserialize
    public List<ListClientResponseData> data;
    public String message;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getClientCount() {
        return clientCount;
    }

    public void setClientCount(String clientCount) {
        this.clientCount = clientCount;
    }

    public  List<ListClientResponseData> getData() {
        return data;
    }

    public void setData(List<ListClientResponseData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
