package com.datafoundry.raven.models;

import java.io.Serializable;

public class User implements Serializable {
	private static final long serialVersionUID = 949866861L;
	public String userId;
	public String userName;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
