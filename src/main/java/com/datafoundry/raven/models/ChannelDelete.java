package com.datafoundry.raven.models;

import javax.validation.constraints.NotNull;

public class ChannelDelete {

    @NotNull
    private String channelId;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    private String clientId;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public ChannelDelete(String channelId) {
        this.channelId = channelId;
    }

    public ChannelDelete() {}

}
