package com.datafoundry.raven.models;

import java.io.Serializable;
import java.util.List;

public class UserGroupResponse implements Serializable {

	private static final long serialVersionUID = 14668534L;

	public Integer statusCode;
	public Integer clientCount;
	public List<UserGroupResponseData> data;
	public String message;

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public Integer getClientCount() {
		return clientCount;
	}

	public void setClientCount(Integer clientCount) {
		this.clientCount = clientCount;
	}

	public List<UserGroupResponseData> getData() {
		return data;
	}

	public void setData(List<UserGroupResponseData> data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
