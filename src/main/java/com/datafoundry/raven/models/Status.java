package com.datafoundry.raven.models;

import java.io.Serializable;

/**
 * @author rama odedara
 * @created on 18/12/2020
 * @description POJO
 * @version number Raven v1.0
 */

public class Status implements Serializable {
	
	private static final long serialVersionUID = 1696875L;

	public static class StatusBuilder {
		private String message;
		private Boolean success;
		private Integer statusCode;

		public StatusBuilder(String message) {
			this.message = message;
		}

		public StatusBuilder isSuccess(Boolean success) {
			this.success = success;
			return this;
		}

		public StatusBuilder withStatusCode(Integer statusCode) {
			this.statusCode = statusCode;
			return this;
		}

        public StatusBuilder(String message, Boolean success, Integer statusCode) {
            this.message = message;
            this.success = success;
            this.statusCode = statusCode;
        }

        public Status build() {
			Status status = new Status();
			status.message = this.message;
			status.statusCode = this.statusCode;
			status.success = this.success;
			return status;
		}
	}

	private String message;
	private Boolean success;
	private Integer statusCode;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	/*
	 * public Status(String message, Boolean success, Integer statusCode) {
	 * this.message = message; this.success = success; this.statusCode = statusCode;
	 * }
	 */

}
