package com.datafoundry.raven.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Varsha
 * @version number Raven v1.0
 * @created 28/12/2020
 * @description POJO for pagination payload
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Pagination {
	private List<String> status;
	private int pageNumber;
	private int pageSize;

	private String clientId;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Pagination() {
	}	

	public Pagination(List<String> status, String clientId, int pageNumber, int pageSize) {
		super();
		this.setStatus(status);
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List<String> getStatus() {
		return status;
	}

	public void setStatus(List<String> status) {
		this.status = status;
	}
}
