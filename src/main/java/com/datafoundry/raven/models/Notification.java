package com.datafoundry.raven.models;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author rama odedara
 * @created on 18/12/2020
 * @description POJO for Notification request
 * @version number Raven v1.0
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Notification implements Serializable {
	
	private static final long serialVersionUID = 1646945L;

	private String clientId;
	private List<String> userIds;
	private String channelId;
	@NotNull
	private String requestingService;
	@NotNull
	private String message;
	@NotNull
	private String messageType;
	@ApiModelProperty(hidden = true)
	private String rrId;
	@ApiModelProperty(hidden = true)
	private String channelName;

	public Notification() {
		super();
	}

	public Notification(String clientId, List<String> userIds, String requestingService, String message, String messageType) {
		this.clientId = clientId;
		this.userIds = userIds;
		this.requestingService = requestingService;
		this.message = message;
		this.messageType = messageType;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public List<String> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<String> userIds) {
		this.userIds = userIds;
	}

	public String getChannelId() {
		return channelId;
	}
	
	public String getRrId() {
		return rrId;
	}

	public void setRrId(String rrId) {
		this.rrId = rrId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public void setRequestingService(String requestingService) {
		this.requestingService = requestingService;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getClientId() {
		return clientId;
	}


	public String getRequestingService() {
		return requestingService;
	}

	public String getMessage() {
		return message;
	}

	public String getMessageType() {
		return messageType;
	}

}
