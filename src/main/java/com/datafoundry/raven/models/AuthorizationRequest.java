package com.datafoundry.raven.models;

import java.io.Serializable;

public class AuthorizationRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9865541L;
	private String emailId;
	private String password;
	private String subdomain;
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSubdomain() {
		return subdomain;
	}
	public void setSubdomain(String subdomain) {
		this.subdomain = subdomain;
	}
	
	
}
