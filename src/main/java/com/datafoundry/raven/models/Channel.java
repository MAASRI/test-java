package com.datafoundry.raven.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.List;

/**
 * @author ram
 * @version number Raven v1.0
 * @created 18/12/2020
 * @description POJO for channel request
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Channel implements Serializable {
	
	private static final long serialVersionUID = 646145L;
    private String clientId;
    @NotNull
    private String requestingService;
    private List<String> userGroup;
    @NotNull
    private String channelName;
    private List<String> users;
    private String description;

    public Channel() {
    }

    public Channel(String clientId, @NotNull String requestingService, List<String> userGroup, List<String> users, @NotNull String channelName, String description) {
        this.clientId=clientId;
        this.requestingService = requestingService;
        this.userGroup = userGroup;
        this.users = users;
        this.channelName = channelName;
        this.description = description;
    }
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRequestingService() {
        return requestingService;
    }

    public void setRequestingService(String requestingService) {
        this.requestingService = requestingService;
    }

    public List<String> getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(List<String> userGroup) {
        this.userGroup = userGroup;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
