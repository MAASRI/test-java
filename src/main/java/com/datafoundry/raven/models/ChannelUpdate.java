package com.datafoundry.raven.models;


import java.util.List;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ram
 * @version number Raven v1.0
 * @created 19/12/2020
 * @description description
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChannelUpdate {
    @NotNull
    private String channelId;
    private List<String> userGroup;
    private List<String> users;
    private String description;
    private String action;
    private String clientId;


    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    public ChannelUpdate() {
    }

    public ChannelUpdate(@NotNull String channelId, List<String> userGroup, List<String> users, String description) {
        this.channelId = channelId;
        this.userGroup = userGroup;
        this.users = users;
        this.description = description;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action.trim();
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId.trim();
    }

    public List<String> getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(List<String> userGroup) {
        this.userGroup = userGroup;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description.trim();
    }
}
