package com.datafoundry.raven.models;

import java.io.Serializable;
import java.util.List;

public class UserGroupResponseData implements Serializable {
	private static final long serialVersionUID = 14668534L;
	public Integer clientId;
	public String userGroupName;
	public List<User> listOfUsers;

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public String getUserGroupName() {
		return userGroupName;
	}

	public void setUserGroupName(String userGroupName) {
		this.userGroupName = userGroupName;
	}

	public List<User> getListOfUsers() {
		return listOfUsers;
	}

	public void setListOfUsers(List<User> listOfUsers) {
		this.listOfUsers = listOfUsers;
	}

}
