package com.datafoundry.raven.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.List;

public class ListClientResponseData implements Serializable {

    private static final long serialVersionUID = 14668534L;

    public String _id;
    public String clientId;
    public String clientName;
    public String industry;
    public String billingAddress;
    public String contactPersonName;
    public String contactPhoneNumber;
    public String emailId;
    public String configParams;
    public List<String> subdomain;
    public List<String> solutionName;
    public String clientLogoURL;
    public String licenseId;
    public String status;
    public String createdBy;
    public String createdTime;
    public String updatedBy;
    public String updatedTime;
    @JsonIgnore
    public String license;

    public String clientLoggedInUrl;
    public String sso;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getConfigParams() {
        return configParams;
    }

    public void setConfigParams(String configParams) {
        this.configParams = configParams;
    }

    public List<String> getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(List<String> subdomain) {
        this.subdomain = subdomain;
    }

    public String getClientLogoURL() {
        return clientLogoURL;
    }

    public void setClientLogoURL(String clientLogoURL) {
        this.clientLogoURL = clientLogoURL;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getClientLoggedInUrl() {
        return clientLoggedInUrl;
    }

    public void setClientLoggedInUrl(String clientLoggedInUrl) {
        this.clientLoggedInUrl = clientLoggedInUrl;
    }

    public String getSso() {
        return sso;
    }

    public void setSso(String sso) {
        this.sso = sso;
    }
}
