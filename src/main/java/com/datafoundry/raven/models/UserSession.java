package com.datafoundry.raven.models;

import java.io.Serializable;

/**
 * @author ram
 * @version number Raven v1.0
 * @created 19/12/2020
 * @description description
 */
public class UserSession implements Serializable {
	
	private static final long serialVersionUID = 365246145L;
    private String firstName;
    private String lastName;
    private boolean access;
    private String clientId;
    private String roleId;
    private String displayName;
    private String roleName;
    private String emailId;
    private String userId;

    public UserSession() {
    }

    public UserSession(String firstName, String lastName, boolean access, String clientId, String roleId, String displayName, String roleName, String emailId, String userId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.access = access;
        this.clientId = clientId;
        this.roleId = roleId;
        this.displayName = displayName;
        this.roleName = roleName;
        this.emailId = emailId;
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean getAccess() {
        return access;
    }

    public void setAccess(boolean access) {
        this.access = access;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
