package com.datafoundry.raven.models;

import java.io.Serializable;
import java.util.List;

public class UserGroupRequest implements Serializable {
	private static final long serialVersionUID = 6456461L;
	private String clientId;
	private List<String> userGroups;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public List<String> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<String> userGroups) {
		this.userGroups = userGroups;
	}


}
