package com.datafoundry.raven.models;

import javax.validation.constraints.NotNull;

public class NotificationUpdate {

    @NotNull
    private String notificationId;
    @NotNull
    private String status;
    private String clientId;
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }


    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public NotificationUpdate(String notificationId, String status) {
        this.notificationId = notificationId;
        this.status = status;
    }

}
