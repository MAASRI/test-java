package com.datafoundry.raven.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * @author ram
 * @version number Raven v1.0
 * @created 19/12/2020
 * @description POJO for persisting Channel to database
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChannelModel implements Serializable {
	
	private static final long serialVersionUID = 164646145L;
	
	@JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    private String clientId;
    private List<String> userGroup;
    private List<String> users;
    private String channelName;
    private String description;
    private String channelId;
    private String status;
    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime updatedAt;
    private String updatedBy;

    public ChannelModel() {
    }

    public ChannelModel(
            String clientId,
            List<String> userGroup,
            List<String> users,
            String channelName,
            String description,
            String channelId,
            String status,
            LocalDateTime createdAt,
            String createdBy) {
        this.clientId = clientId;
        this.userGroup = userGroup;
        this.users = users;
        this.channelName = channelName;
        this.description = description;
        this.channelId = channelId;
        this.status = status;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }


	public List<String> getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(List<String> userGroup) {
		this.userGroup = userGroup;
	}

	public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
