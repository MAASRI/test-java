package com.datafoundry.raven.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * @author rama odedara
 * @created on 18/12/2020
 * @description POJO for persisting Notification to database
 * @version number Raven v1.0
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationModel implements Serializable {

	private static final long serialVersionUID = 65465461L;
	@JsonSerialize(using = ToStringSerializer.class)
	private ObjectId id;
	private String notificationId;
	private String clientId;
	private String userId;
	private String requestingService;
	private String message;
	private String messageType;
	private String status;
	private String channelId;
	private String channelName;
	private LocalDateTime expiryTime;
	private LocalDateTime createdAt;
	private String createdBy;
	private LocalDateTime updatedAt;
	private String updatedBy;
	private LocalDateTime deliveredTime;

	public NotificationModel() {
	}

	public NotificationModel(ObjectId id, String notificationId, String clientId, String userId,
			String requestingService, String message, String messageType, String status, String channelId,
			String channelName, LocalDateTime expiryTime, LocalDateTime createdAt, String createdBy) {
		this.id = id;
		this.notificationId = notificationId;
		this.clientId = clientId;
		this.userId = userId;
		this.requestingService = requestingService;
		this.message = message;
		this.messageType = messageType;
		this.status = status;
		this.expiryTime = expiryTime;
		this.channelId = channelId;
		this.channelName = channelName;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getChannelName() {
		return channelName;
	}

	public LocalDateTime getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(LocalDateTime expiryTime) {
		this.expiryTime = expiryTime;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setRequestingService(String requestingService) {
		this.requestingService = requestingService;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setDeliveredTime(LocalDateTime deliveredTime) {
		this.deliveredTime = deliveredTime;
	}

	public ObjectId getId() {
		return id;
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public String getUserId() {
		return userId;
	}

	public String getRequestingService() {
		return requestingService;
	}

	public String getMessage() {
		return message;
	}

	public String getMessageType() {
		return messageType;
	}

	public String getStatus() {
		return status;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public LocalDateTime getDeliveredTime() {
		return deliveredTime;
	}

	public String getClientId() {
		return clientId;
	}

}
