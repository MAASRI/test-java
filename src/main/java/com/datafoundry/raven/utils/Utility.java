package com.datafoundry.raven.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.raven.entity.NotificationModel;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.models.Channel;
import com.datafoundry.raven.models.ChannelDelete;
import com.datafoundry.raven.models.ChannelUpdate;
import com.datafoundry.raven.models.Notification;
import com.datafoundry.raven.models.NotificationUpdate;
import com.datafoundry.raven.models.Pagination;
import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.models.UserSession;
import com.datafoundry.raven.service.impl.NotificationServiceImpl;

/**
 * @author akhilesh
 * @version number Raven v1.0
 * @created on 18/12/2020
 * @description Class for managing common utility methods for the application
 */
public class Utility {

	private static RRAdapter logger = new RRAdapterImpl(NotificationServiceImpl.class.getName());

	public static String getFirstErrorInformation(BindingResult bindingResult, String rrId) {
		String fieldName = null;
		logger.debug(rrId, AppConstants.RAVEN, "++----Utility Methods--getErrorInformation()---++", null);
		StringBuilder sb = new StringBuilder();
		for (Object object : bindingResult.getAllErrors()) {
			if (object instanceof FieldError) {
				FieldError fieldError = (FieldError) object;
				sb.append(fieldError.getDefaultMessage().trim());
				fieldName = fieldError.getField();
				logger.debug(rrId, AppConstants.RAVEN, "FIELD ERROR_" + fieldError.getField(), null);
				break;
			}

			if (object instanceof ObjectError) {
				ObjectError objectError = (ObjectError) object;
				sb.append(objectError.getObjectName());
				logger.debug(rrId, AppConstants.RAVEN, "OBJECT_ERROR_" + objectError.getObjectName(), null);
				break;
			}

		}

		return fieldName + " " + sb.toString();
	}

	public static void validateUUIDs(List<String> UUIDs) throws AppException {
		for (String id : UUIDs) {
			if (id != null) {
				try {
					UUID.fromString(id);
				} catch (Exception ex) {
					throw new AppException(AppConstants.INVALID_UUID, AppConstants.INVALID_UUID_TEXT);
				}
			}
		}
	}

	public static String createNotificationId(String clientId) {
		return clientId + UUID.randomUUID();
	}

	public static String getExceptionMessage(Exception ex) {
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

	public static String getRRId(String rrId) {
		if (rrId == null || rrId.isEmpty())
			return UUID.randomUUID().toString();
		else
			return rrId;
	}

	public static void validateCreateNotificationModel(Notification notification, UserSession userSession) {
		validateUUIDs(List.of(notification.getRrId()));
		if (!(notification.getMessageType().equals(AppConstants.USER_MSG)
				|| notification.getMessageType().equals(AppConstants.CHANNEL_MSG))) {
			throw new AppException(AppConstants.INCORRECT_MSG_TYPE, AppConstants.INCORRECT_MSG_TYPE_TXT);
		}
		if (notification.getMessageType().equals(AppConstants.USER_MSG) && notification.getUserIds() == null) {
			throw new AppException(AppConstants.UNPROCESSABLE_ENTITY, AppConstants.USER_ID_NOT_FOUND_TXT);
		}
		if (notification.getMessageType().equals(AppConstants.USER_MSG) && notification.getUserIds().isEmpty()) {
			throw new AppException(AppConstants.UNPROCESSABLE_ENTITY, AppConstants.USER_ID_NOT_FOUND_TXT);
		}
		if (notification.getMessageType().equals(AppConstants.CHANNEL_MSG) && notification.getChannelId() == null) {
			throw new AppException(AppConstants.UNPROCESSABLE_ENTITY, AppConstants.INVALID_CHANNEL_ID_TXT);
		}
		if (notification.getMessageType().equals(AppConstants.CHANNEL_MSG) && (notification.getChannelId().isEmpty() || notification.getChannelId().isBlank())) {
			throw new AppException(AppConstants.CHANNEL_NOT_FOUND, AppConstants.INVALID_CHANNEL_ID_TXT);
		}
		if (notification.getMessageType().equals(AppConstants.USER_MSG) && userSession == null) {
			throw new AppException(AppConstants.INVALID_DATA, AppConstants.USER_ID_NOT_FOUND_TXT);
		}
		if(notification.getMessage()==null ||notification.getMessage().isBlank() || notification.getMessage().isEmpty()) {
			throw new AppException(AppConstants.UNPROCESSABLE_ENTITY, AppConstants.MESSAGE_MUST_NOT_BE_EMPTY);
		}
		if(notification.getRequestingService()==null ||notification.getRequestingService().isBlank() || notification.getRequestingService().isEmpty()) {
			throw new AppException(AppConstants.UNPROCESSABLE_ENTITY, AppConstants.REQUESTING_SERVICE_MUST_NOT_BE_EMPTY);
		}
	}

	public static void validateCreateChannelModel(Channel channel, String rrId) {
		validateUUIDs(List.of(rrId));
		if (channel.getChannelName().trim().isEmpty())
			throw new AppException(AppConstants.INCORRECT_CHANNEL_NAME, AppConstants.INVALID_CHANNEL_NAME);
		if (channel.getRequestingService().trim().isEmpty())
			throw new AppException(AppConstants.INCORRECT_REQUESTING_SERVICE, AppConstants.INVALID_REQUESTING_SERVICE);
		if (isListNotEmpty(channel.getUsers()))
			channel.setUsers(channel.getUsers().stream().distinct().collect(Collectors.toList()));
		if (isListNotEmpty(channel.getUserGroup()))
			channel.setUserGroup(channel.getUserGroup().stream().distinct().collect(Collectors.toList()));

	}

	public static void validateUserSession(UserSession userSession) {
		if (userSession == null || userSession.getUserId() == null)
			throw new AppException(AppConstants.INVALID_DATA, AppConstants.USER_ID_NOT_FOUND_TXT);
		else if(userSession.getUserId().isEmpty())
			throw new AppException(AppConstants.USER_ID_NOT_FOUND, AppConstants.USER_ID_NOT_FOUND_TXT);
	}

	public static void validateNotificationUpdateModelStatus(NotificationUpdate notificationUpdate) {
		if (notificationUpdate.getNotificationId().isEmpty()) {
			throw new AppException(AppConstants.INVALID_UPDATE_STATUS, AppConstants.INVALID_NOTIFICATION_ID_TEXT);
		}
		if (notificationUpdate.getStatus().isEmpty()) {
			throw new AppException(AppConstants.INVALID_UPDATE_STATUS, AppConstants.INVALID_STATUS_TEXT);
		}
		if (!AppConstants.UPDATE_STATUS.contains(notificationUpdate.getStatus())) {
			throw new AppException(AppConstants.INVALID_UPDATE_STATUS,
					AppConstants.INVALID_NOTIFICATION_UPDATE_STATUS_TEXT);
		}

	}

	public static void validateChannelDeleteStatus(ChannelDelete channelDelete) {
		if (channelDelete.getChannelId().isEmpty()) {
			throw new AppException(AppConstants.INVALID_UPDATE_STATUS, AppConstants.INVALID_CHANNEL_ID_TEXT);
		}
	}

    public static final Status INSERT_RESPONSE = new Status.StatusBuilder(AppConstants.CREATED_SUCCESSFULLY, true, 201).build();
    public static final Status UPDATE_RESPONSE = new Status.StatusBuilder(AppConstants.UPDATED_SUCCESSFULLY, true, 200).build();
    public static final Status ERROR_RESPONSE = new Status.StatusBuilder(AppConstants.SOMETHING_WENT_WRONG, false, 500).build();

	public static String randomInt() {
		Random random = new Random();
		return String.format("%04d", random.nextInt(10000));
	}

	public static String getChannelId(String clientId) {
		return clientId + randomInt();
	}

	public static LocalDateTime getUTCLocalDateTime() {
		return LocalDateTime.now(ZoneOffset.UTC);
	}

	public static Status insertResponse(boolean ack) {
		if (ack)
			return INSERT_RESPONSE;
		else
			return ERROR_RESPONSE;
	}

	public static Integer findOffSet(Pagination pageable) {
		int skips = 0;
		if (pageable.getPageSize() > 0 && pageable.getPageNumber() > 0)
			skips = pageable.getPageSize() * (pageable.getPageNumber() - 1);
		return skips;
	}

	public static void getPageNumber(Pagination req) {
		if (req.getPageNumber() < 1)
			req.setPageNumber(1);
	}

	public static void getPageSize(Pagination req) {
		if (req.getPageSize() < 1)
			req.setPageSize(10);
	}

	

	public static NotificationModel createNotificationDBModel(Notification notification,String userId ,Long expiryHours) {
		LocalDateTime expiryTime = (expiryHours == null) ? null
				: LocalDateTime.now(ZoneOffset.UTC).plusHours(expiryHours);
		return new NotificationModel(new ObjectId(), Utility.createNotificationId(notification.getClientId()),
				notification.getClientId(), userId, notification.getRequestingService(),
				notification.getMessage(), notification.getMessageType(), AppConstants.UN_DELIVERED,
				notification.getChannelId(), notification.getChannelName(), expiryTime,
				LocalDateTime.now(ZoneOffset.UTC), notification.getRequestingService());
	}

	public static void validateChannelUpdate(ChannelUpdate channelUpdate) {
		if (channelUpdate.getUserGroup() != null)
			channelUpdate.setUserGroup(channelUpdate.getUserGroup().stream().filter(item -> item != null && !item.isEmpty()).collect(Collectors.toList()));
		if (channelUpdate.getUsers() != null)
			channelUpdate.setUsers(channelUpdate.getUsers().stream().filter(item -> item != null && !item.isEmpty()).collect(Collectors.toList()));
		if (channelUpdate.getChannelId().trim().isEmpty())
			throw new AppException(AppConstants.INVALID_CHANNEL_ID, AppConstants.INVALID_CHANNEL_ID_TXT);
		else if (channelUpdate.getDescription() == null && (channelUpdate.getUsers()==null || channelUpdate.getUsers().isEmpty()) && (channelUpdate.getUserGroup()== null || channelUpdate.getUserGroup().isEmpty()))
			throw new AppException(AppConstants.INVALID_DATA, AppConstants.NOTHING_TO_UPDATE);
	}

	public static void validateStatus(Pagination req, List<String>listOfStatus) {
		if (req.getStatus()!=null) {
			if (!listOfStatus.containsAll(req.getStatus())) {
				throw new AppException(SwaggerConstants.UNPROCESSABLE_ENTITY, AppConstants.INVALID_STATUS);
			}
		}
	}
	public static boolean isListNotEmpty(List<String> list){
		return list!=null && !list.isEmpty();
	}
	public static boolean isStringEmpty(String str){
		return str==null || str.isEmpty();
	}
	public static String getClientId(String clientId,UserSession userSession,String defaultDB) {
		if (!isStringEmpty(clientId)) return clientId;
		else if (userSession !=null && !isStringEmpty(userSession.getClientId()))
			return userSession.getClientId();
		else return defaultDB;
	}
//	public static String getClientId(UserSession userSession,String defaultDB) {
//		if (userSession !=null && !isStringEmpty(userSession.getClientId()))
//			return userSession.getClientId();
//		else return defaultDB;
//	}
}
