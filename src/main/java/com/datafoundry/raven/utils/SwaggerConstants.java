package com.datafoundry.raven.utils;

public class SwaggerConstants {

	public static final String CREATE_NOTIFICATION_TITLE = "To Create Notification";
	public static final String NOTIFICATION_CREATED_SUCCESS_MESSAGE = "Notification created";
    public static final String READ_NOTIFICATION_BYSTATUS_TITLE = "Read Notification by Status";
	public static final String CREATE_NOTIFICATION_SUCCESS_RESPONSE = "{\"message\":\"Created successfully\",\"success\":true,\"statusCode\":201}";
	public static final String UPDATE_NOTIFICATION_TITLE = "To Update Notification Status";
	public static final String BAD_REQUEST = "Bad Request!!";
	public static final String UPDATE_EXPIRED_NOTIFICATION_TITLE = "To Delete Expired Channel Notifications";
	public static final String NOTIFICATION_UPDATED_SUCCESS_MESSAGE = "Notification updated";
	public static final String UPDATE_NOTIFICATION_SUCCESS_RESPONSE = "{\"message\":\"Updated successfully\",\"success\":true,\"statusCode\":200}";
	public static final String UNPROCESSABLE_ENTITY = "Unprocessable Entity";
	public static final String NOT_FOUND = "Not Found";
	public static final String NOTIFICATION_PUT_422_RESPONSE = "{\"message\":\"Please provide proper status (either read/deleted)\",\"success\":false,\"statusCode\":422}";
	public static final String NOTIFICATION_PUT_404_RESPONSE = "{\"message\":\"Notificaion not found\",\"success\":false,\"statusCode\":404}";
	public static final String NOTIFICATION_Post_404_RESPONSE = "{\"message\":\"Channel not found\",\"success\":false,\"statusCode\":404}";
	public static final String NOTIFICATION_PUT_400_RESPONSE = "{\"message\":\"userId cannot be null or empty\",\"success\":false,\"statusCode\":400}";
	public static final String NOTIFICATION_POST_422_RESPONSE = "{\"message\":\"messageType must not be empty\",\"success\":false,\"statusCode\":422}";
	public static final String NOTIFICATION_POST_400_RESPONSE = "{\"message\":\"messageType must not be null\",\"success\":false,\"statusCode\":400}";

	public static final String CREATE_CHANNEL_TITLE = "To Create Channel";
	public static final String CREATE_SUCCESS_RESPONSE = "{\"message\":\"Created successfully\", \"success\":true, \"statusCode\":201}";
	public static final String POST_400_RESPONSE = "{\"message\":\"requestingService must not be null\", \"success\":false, \"statusCode\":400}";
	public static final String POST_422_RESPONSE = "{\"message\":\"channelName must not be null or empty\", \"success\":false, \"statusCode\":422}";
	public static final String APPLICATION_JSON_VALUE = "application/json";

	public static final String DELETE_CHANNEL_TITLE = "To Delete Channel";
	public static final String DELETE_SUCCESS_RESPONSE = "{\"message\":\"Deleted successfully\", \"success\":true, \"statusCode\":200}";
	public static final String DELETE_404_RESPONSE = "{\"message\":\"Channel not found\", \"success\":false, \"statusCode\":404}";
	public static final String DELETE_400_RESPONSE = "{\"message\":\"userId cannot be null or empty\", \"success\":false, \"statusCode\":400}";
	public static final String DELETE_422_RESPONSE = "{\"message\":\"channelId cannot be empty\",\"success\":false,\"statusCode\":422}";

	public static final String GET_UNDELIVERED_NOTIFICATIONS_TITLE = "Get all undelivered notifications and update status of each notification to delivered";
	public static final String NOTIFICATIONS_FOUND_SUCCESS_MESSAGE = "Notifications found";
	public static final String NOTIFICATION_GET_ALL_UNDELIVERED_400_RESPONSE = "{\"errorCode\":\"INVALID_UUID\",\"errorMessage\":\"Improper UUID is passed\"}";
	public static final String UNDELIVEDRED_NOTIFICATIONS_NOT_FOUND = "{\"errorCode\":\"INVALID_DATA\",\"errorMessage\":\"clientId must not be null\"}";


    public static final String UNDELIVERED_NOTIFICATION_SUCCESS_RESPONSE = "[\r\n" + 
    		"    {\r\n" + 
    		"        \"id\": \"5ffc4707bef6db2f9c7ad908\",\r\n" + 
    		"        \"notificationId\": \"123402f29625-69e8-4df4-8678-90167fc68db8\",\r\n" + 
    		"        \"clientId\": \"1234\",\r\n" + 
    		"        \"userId\": \"4567\",\r\n" + 
    		"        \"requestingService\": \"cm-dm-service\",\r\n" + 
    		"        \"message\": \"Your case is created successfuly with the id 1234-2048-3856\",\r\n" + 
    		"        \"messageType\": \"user-msg\",\r\n" + 
    		"        \"status\": \"unDelivered\",\r\n" + 
    		"        \"createdAt\": \"2021-01-11T12:39:35.847\",\r\n" + 
    		"        \"createdBy\": \"cm-dm-service\"\r\n" + 
    		"    }\r\n" + 
    		"]";
    

    public static final String READ_NOTIFICATION_SUCCESS_RESPONSE = "[\r\n" + 
    		"    {\r\n" + 
    		"        \"id\": \"5ffc4707bef6db2f9c7ad908\",\r\n" + 
    		"        \"notificationId\": \"123402f29625-69e8-4df4-8678-90167fc68db8\",\r\n" + 
    		"        \"clientId\": \"1234\",\r\n" + 
    		"        \"userId\": \"444\",\r\n" + 
    		"        \"requestingService\": \"cm-dm-service\",\r\n" + 
    		"        \"message\": \"Your case is created successfuly with the id 1234-2048-3856\",\r\n" + 
    		"        \"messageType\": \"user-msg\",\r\n" + 
    		"        \"status\": \"delivered\",\r\n" +
    		"        \"channelId\": \"12349628\",\r\n" +
    		"        \"createdAt\": \"2021-01-11T12:39:35.847\",\r\n" + 
    		"        \"createdBy\": \"cm-dm-service\",\r\n" + 
    		"        \"updatedAt\":\"2021-01-06T17:57:45.632\",\r\n" +
    		"        \"updatedBy\":\"444\",\r\n" +
    		"        \"deliveredTime\":\"2021-01-06T17:57:45.632\",\r\n" +
    		"    }\r\n" + 
    		"]";

    public static final String NOTIFICATION_GET_400_RESPONSE = "{\"message\":\"userId cannot be null or empty\",\"success\":false,\"statusCode\":400}";
   	public static final String SUCCESS = "Success";
   	public static final String NO_CONTENT = "No Content";
	public static final String RESPONSE_422 = "{\"message\":\"Please provide proper status\",\"success\":false,\"statusCode\":422}";

	public static final String RR_ID_EXAMPLE = "d300458c-946a-48a9-ba6c-04147b9b1a75";
	public static final String USER_SESSION_EXAMPLE = "{\"firstName\": \"\",\"lastName\": \"\",\"access\": true,\"clientId\": \"1234\",\"roleId\": \"12341117\",\"displayName\": \"Shravan G\",\"roleName\": \"dfAdmin\",\"emailId\": \"shravan.g@datafoundry.ai\",\"userId\": \"12344510\"}";

	public static final String UPDATE_CHANNEL_TITLE = "To Update Channel";
	public static final String UPDATE_SUCCESS_RESPONSE = "{\"message\":\"Updated Successfully\", \"success\":true, \"statusCode\":200}";
	public static final String UPDATE_CHANNEL_400_RESPONSE = "{\"message\":\"channelId must not be null\", \"success\":false, \"statusCode\":400}";
	public static final String UPDATE_CHANNEL_422_RESPONSE = "{\"message\":\"channelId must not be null or empty\", \"success\":false, \"statusCode\":422}";

    public static final String VIEW_CHANNEL_TITLE = "View Channels";
	public static final String VIEW_CHANNEL_SUCCESS_RESPONSE = "[{\"id\": \"5ff61523d4f52c03f924744b\",\"clientId\": \"1234\",\"userGroup\": [\"123456\",\"123457\"],\"users\":[\"123\",\"124\"],\"channelName\": \"data-entry\",\"description\": \"Data Entry channel\",\"channelId\": \"12340746\",\"status\": \"active\",\"createdAt\": \"2021-01-06T19:53:07.566\",\"createdBy\": \"microservice\"}]";
	public static final String CHANNEL_422_RESPONSE = "{\"message\":\"Please provide valid status (either active/inactive/deleted) or valid request body\",\"success\":false,\"statusCode\":422}";
	public static final String CHANNEL_400_RESPONSE = "{\"message\":\"Invalid userSession/requestBody\",\"success\":false,\"statusCode\":400}";
	public static final String NO_CONTENT_RESPONSE = "{\"statusCode\":204,\"message\":\" \", \"success\":\"false\"}";
	public static final String UPDATE_EXPIRED_NOTIFICATION_SUCCESS_RESPONSE = "{\"message\":\"Request received!\",\"success\":true,\"statusCode\":200}";

}
