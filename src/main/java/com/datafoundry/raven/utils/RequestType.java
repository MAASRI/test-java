package com.datafoundry.raven.utils;

public enum RequestType {
	GET_USER_IDS,
	GET_AUTH_TOKEN,
	GET_CLIENTS
}
