package com.datafoundry.raven.utils;

import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.models.UserSession;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


/**
 * @author ram
 * @version number Raven v1.0
 * @created 21/12/2020
 * @description Json header converter to convert string to UserSession
 */
@Component
public class UserSessionConverter implements Converter<String, UserSession> {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public UserSession convert(String userSession) throws AppException {
        try {
            if (userSession.trim().isEmpty()) return null;
            else
                return objectMapper.readValue(userSession, UserSession.class);
        } catch (Exception e) {
            return null;
        }
    }
}
