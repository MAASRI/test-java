package com.datafoundry.raven.utils;

import java.util.Arrays;
import java.util.List;

/**
 * @author akhilesh
 * @version number Raven v1.0
 * @created on 18/12/2020
 * @description Class for managing constants for the application
 */
public class AppConstants {

    // path constants
    public static final String NOTIFICATION_PATH = "/notification";
    public static final String STATUS_PATH = "/status";
    public static final CharSequence API_DOCS_PATH = "/api-docs";
    public static final String CHANNEL_PATH = "/channel";
    public static final String CREATE_PATH = "/create";
    public static final String UPDATE_PATH = "/update";
    public static final String GET_PATH = "/get";
    public static final String DELETE_PATH = "/delete";
    public static final String READ_PATH = "/read";
    public static final String GET_NOTIFICATION = "/getUnDelivered";
    public static final String VIEW_PATH = "/view";
    public static final String UPDATE_EXPIRED = "/updateExpired";

	// programming constants
	public static final String APPLICATION_RUNNING = "Application is running!";
	public static final String INVALID_USER_SESSION = "userId cannot be null or empty";
	public static final String NOTIFICATION_ID_NOT_FOUND = "Notification not found";
	public static final List<String> UPDATE_STATUS = Arrays.asList("read", "deleted");

    public static final String UPDATED_ONLY_THE_STATUS_SUCCESSFULLY = "Updated only the status successfully";
    public static final String INVALID_UPDATE_STATUS = "INVALID_UPDATE_STATUS";
    public static final String INVALID_NOTIFICATION_ID_TEXT = "notificationId cannot be empty";
    public static final String INVALID_STATUS_TEXT = "status cannot be empty";
    public static final String INVALID_CHANNEL_ID_TEXT = "channelId cannot be empty";
	// collections
	public static final String NOTIFICATION_COLLECTION = "notifications";
	public static final String CHANNEL_COLLECTION = "channel";
    public static final String UPDATED_SUCCESSFULLY = "Updated Successfully";
    public static final String CHANNEL_ID_NOT_FOUND = "Channel not found";



    //Response's
    public static final String CREATED_SUCCESSFULLY = "Created successfully";
    public static final String INCORRECT_MSG_TYPE_TXT = "messageType must not be empty";
    public static final String INVALID_UUID_TEXT = "Improper UUID is passed";
    public static final String CHANNEL_ID = "channelId";
	public static final String SOMETHING_WENT_WRONG = "Something went wrong!!, Please try again later";
    public static final String USER_ID_NOT_FOUND_TXT = "userId cannot be null or empty";

    public static final String RR_ID_UNAVAILABLE_TEXT = "rrId is not passing in the request";
    public static final String USER_SESSION = "userSession";
    public static final String STATUS = "status";
    public static final String DELETED = "deleted";
    public static final String HEADER = "header";
    public static final String DELETED_SUCCESSFULLY = "Deleted successfully";

    public static final String USER_SESSION_INVALID = "userSession must not null or invalid ";
    public static final String INVALID_CHANNEL_NAME = "channelName cannot be null or empty";
    public static final String INVALID_REQUESTING_SERVICE = "requestingService cannot be null or empty";
    public static final String INVALID_STATUS = "Please provide proper status";
    public static final String NO_DATA_FOUND = "No data found !";
    public static final String INVALID_DATA_TEXT = "Invalid data passed in request";
    public static final String INVALID_NOTIFICATION_UPDATE_STATUS_TEXT = "Please provide proper status (either read/deleted)";
    public static final String NOTHING_TO_UPDATE = "Nothing to update";
    public static final String INVALID_USERGROUP = "userGroup doesn't exists";
    public static final String INVALID_USERS = 	"user doesn't exists";
   

    //Constants & Fields
    public static final String CLIENT_ID = "clientId";
    public static final String ACTIVE = "active";
    public static final String NOTIFICATION_ID = "notificationId";
    public static final String UPDATED_BY = "updatedBy";
    public static final String UPDATED_AT = "updatedAt";
    public static final String UN_DELIVERED = "unDelivered";
    public static final String DELIVERED_TIME = "deliveredTime";
    public static final String USER_MSG = "user-msg";
    public static final Object CHANNEL_MSG = "channel-msg";
    public static final String DELIVERED = "delivered";
    public static final String USER_ID = "userId";
    public static final String _ID = "_id";
    public static final String MESSAGE_TYPE = "messageType";
    public static final String EXPIRED_TIME = "expiryTime";
    public static final String SCHEDULED_UPDATE = "scheduled-update";
    public static final String RR_ID = "rrId";
    public static final String RAVEN = "RAVEN";
    public static final String USERS = "users";
    public static final String USER_GROUP = "userGroup";
    public static final String DESCRIPTION = "description";
    public static final String ADD = "add";
    public static final String DELETE = "delete";


    //Exceptions
    public static final String NO_DATA = "NO_DATA";
    public static final String NOT_FOUND = "NOT_FOUND";
    public static final String INVALID_DATA = "INVALID_DATA";
    public static final String ALREADY_EXISTS = "ALREADY_EXISTS";
    public static final String INVALID_UUID = "INVALID_UUID";
    public static final String USER_ID_NOT_FOUND = "USER_ID_NOT_FOUND";
    public static final String SERVICE_UNAVAILABLE = "SERVICE_UNAVAILABLE";
    public static final String INCORRECT_REQUESTING_SERVICE = "INCORRECT_REQUESTING_SERVICE";
    public static final String INCORRECT_CHANNEL_NAME = "INCORRECT_CHANNEL_NAME";
    public static final String INCORRECT_MSG_TYPE = "INCORRECT_MSG_TYPE";
    public static final String UNPROCESSABLE_ENTITY = "UNPROCESSABLE_ENTITY";
    public static final String CHANNEL_NOT_FOUND = "CHANNEL_NOT_FOUND";
    public static final String CHANNEL_NOT_FOUND_TXT = "Channel not found";
    public static final String CHANNEL_NAME_NOT_UPDATED = "CHANNEL_NAME_NOT_UPDATED";
    public static final String CHANNEL_NAME_NOT_UPDATED_TXT = "channelName cannot be updated";
    public static final String CHANNEL_STATUS_NOT_UPDATED = "CHANNEL_STATUS_NOT_UPDATED";
    public static final String CHANNEL_STATUS_NOT_UPDATED_TXT = "status cannot be updated";

	public static final String USER_SESSION_MUST_NOT_BE_NULL_OR_INVALID = "userSession must not be null or invalid";
    public static final String MISSING_REQUEST_HEADERS = "Missing Request Headers";
    public static final String INVALID_CHANNEL_ID = "INVALID_CHANNEL_ID";
    public static final String INVALID_CHANNEL_ID_TXT = "channelId must not be empty";
    public static final String REQUESTING_SERVICE_MUST_NOT_BE_EMPTY = "requestingService must not be empty";
    public static final String MESSAGE_MUST_NOT_BE_EMPTY = "message must not be empty";


    public static final String NO_CLIENTS_FOUND_TO_UPDATE_EXPIRED_CHANNEL_NOTIFICATIONS = "No clients found to update expired channel notifications";
    public static final List<String> CHANNEL_STATUS_LIST = Arrays.asList("active", "inactive", "deleted");
    public static final List<String> NOTIFICATION_STATUS_LIST = Arrays.asList("deleted","unDelivered", "delivered","read");
    public static final List<String> CHANNEL_ACTIVE_STATUS_LIST = Arrays.asList("inactive", "deleted");

}
