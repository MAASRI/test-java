package com.datafoundry.raven.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.datafoundry.raven.models.*;
import com.datafoundry.raven.service.impl.RestService;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.datafoundry.raven.Mocks;
import com.datafoundry.raven.entity.NotificationModel;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.repositories.MongoRepository;
import com.datafoundry.raven.service.impl.NotificationServiceImpl;
import com.datafoundry.raven.utils.AppConstants;


/**
 * @author akhilesh
 * @version number Raven v1.0
 * @created on 18/12/2020
 * @description Class for testing NotificationService
 */
@ExtendWith(MockitoExtension.class)
public class NotificationServiceTest {

    @Mock
    MongoRepository<NotificationModel> mongoRepository;
    @Mock
    AsyncNotificationService asyncNotificationService;

    @InjectMocks
    NotificationService notificationService = new NotificationServiceImpl();

    @InjectMocks
    RestService restService;

    @Test
	public void createNotification() throws AppException {

		Notification notification = new Notification(Mocks.CLIENT_ID, Collections.singletonList(Mocks.USER_ID), Mocks.CM_DM_SERVICE,
				"Your request has been approved", Mocks.USER_MSG_TYPE);
		notificationService.create(notification);
	}


    @Test
    public void getUnDeliveredNotifications() throws AppException {
        UserSession userSession = new UserSession();
        userSession.setClientId(Mocks.CLIENT_ID);
        userSession.setUserId(Mocks.USER_ID);
        Pagination pagination = new Pagination(null,"1234", 1, 5);
        ArrayList<NotificationModel> notificationModelList = new ArrayList<>();
        Mockito.when(mongoRepository.findAll(any(),any(),any(),any(),any(),any())).thenReturn(notificationModelList);

        List<NotificationModel> notificationList = notificationService.getUnDeliveredNotifications(userSession,
                Mocks.RR_ID, pagination);

        Assertions.assertEquals(0, notificationList.size());

    }

    @Test
    public void updateNotification_Success() {
        Long updatedCount = 1L;
        when(mongoRepository.updateOne(Mockito.any(Bson.class), Mockito.any(Bson.class), Mockito.any(String.class),
                Mockito.any(String.class), Mockito.eq(NotificationModel.class)))
                .thenReturn(updatedCount);
        Status result = notificationService.statusUpdate(Mocks.userSession, Mocks.notificationUpdate, Mocks.RR_ID);
        Assertions.assertEquals(result.getSuccess(), true);
    }

    @Test
    public void updateNotification_Failure_NotificationNotFound() {
        Long updatedCount = 0L;
        when(mongoRepository.updateOne(Mockito.any(Bson.class), Mockito.any(Bson.class), Mockito.any(String.class),
                Mockito.any(String.class), Mockito.eq(NotificationModel.class)))
                .thenReturn(updatedCount);

        Throwable exception = Assertions.assertThrows(AppException.class, () -> notificationService.statusUpdate(Mocks.userSession, Mocks.notificationUpdate, Mocks.RR_ID));

        Assertions.assertEquals(exception.getMessage(), AppConstants.NOTIFICATION_ID_NOT_FOUND);
    }

	@Test
	public void readNotificationByStatus() throws Exception {
		Pagination request = new Pagination();
		List<NotificationModel> notificationModels = new ArrayList<NotificationModel>();
		NotificationModel notificationModel = new NotificationModel(new ObjectId(),
				"1234834b5019-6730-4b11-b8b9-fe0803b0d4dc", Mocks.CLIENT_ID, Mocks.USER_ID, Mocks.CM_DM_SERVICE,
				"Your case is created successfuly with the id 1234-2048-3856", Mocks.USER_MSG_TYPE, AppConstants.UN_DELIVERED,null,null,
				null, null, Mocks.CM_DM_SERVICE);

		notificationModels.add(notificationModel);
		Mockito.when(
				mongoRepository.filterDocuments(any(Bson.class), anyInt(), anyInt(), anyString(), anyString(), any()))
				.thenReturn(notificationModels);

		Assertions.assertEquals(
				notificationService.readNotificationsByStatus(Mocks.userSession, request, Mocks.RR_ID),
				notificationModels);
	}

    @Test
    public void deleteExpiredGetResponseException() {
        Throwable exception = Assertions.assertThrows(AppException.class, () -> restService.getResponse(AppConstants.RR_ID, null, null, null, null, ListClientResponse.class));
        Assertions.assertEquals(exception.getMessage(), AppConstants.SERVICE_UNAVAILABLE);
    }

    @Test
    public void deleteExpiredNotificationServiceException() {
        Throwable exception = Assertions.assertThrows(AppException.class, () -> notificationService.updateExpiredChannel(AppConstants.RR_ID));
        Assertions.assertEquals(exception.getMessage(), AppConstants.SERVICE_UNAVAILABLE);
    }
}
