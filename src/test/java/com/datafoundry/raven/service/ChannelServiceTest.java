package com.datafoundry.raven.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bson.conversions.Bson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.datafoundry.raven.Mocks;
import com.datafoundry.raven.entity.ChannelModel;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.models.Channel;
import com.datafoundry.raven.models.ChannelDelete;
import com.datafoundry.raven.models.Notification;
import com.datafoundry.raven.models.Pagination;
import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.models.UserSession;
import com.datafoundry.raven.repositories.MongoRepository;
import com.datafoundry.raven.service.impl.ChannelServiceImpl;
import com.datafoundry.raven.utils.AppConstants;
import com.mongodb.client.MongoClient;

/**
 * @author ram
 * @version number Raven v1.0
 * @created 24/12/2020
 * @description Class for testing ChannelService
 */
@ExtendWith(MockitoExtension.class)
public class ChannelServiceTest {
	@Mock
	MongoClient client;

	@Mock
	MongoRepository<ChannelModel> mongoRepository;
	@InjectMocks
	ChannelService channelService = new ChannelServiceImpl();
	@Mock
	NotificationService notificationService;

	@Mock
	AsyncNotificationService asyncNotificationService;

	@Tag("Create channel")
	@DisplayName("Create Channel Test")
	@Test
	public void createChannel() throws Exception {
		ChannelModel channelModel = new ChannelModel();
		channelModel.setChannelId(Mocks.CLIENT_ID);
		when(mongoRepository.getDatabaseName("")).thenReturn("1234");
		when(mongoRepository.save(any(), any(), any(), any())).thenReturn(true);
		Channel channel = new Channel("", Mocks.CM_DM_SERVICE, Collections.emptyList(), Collections.emptyList(),
				"general", "general channel");
		Assertions.assertEquals(mongoRepository.getDatabaseName(""), "1234");
		Assertions.assertTrue(channelService.create(channel, Mocks.RR_ID));
	}

	@Test
	public void deleteChannel_Success() throws AppException {
		ChannelDelete requestBody = new ChannelDelete("12345678");
		Long updatedCount = 1L;
		when(mongoRepository.updateOne(Mockito.any(Bson.class), Mockito.any(Bson.class), Mockito.any(String.class),
				Mockito.any(String.class), Mockito.eq(ChannelModel.class))).thenReturn(updatedCount);
		Mockito.doNothing().when(asyncNotificationService).updateAllNotificationStatusByChannelId(
				Mockito.any(UserSession.class), Mockito.any(String.class), Mockito.any(String.class),
				Mockito.any(String.class));
		Status result = channelService.delete(Mocks.userSession, requestBody, "rrId");
		Assertions.assertEquals(result.getSuccess(), true);
	}

	@Test
	public void deleteChannel_Failure_ChannelNotFound() throws AppException {
		ChannelDelete requestBody = new ChannelDelete("12345678");
		Long updatedCount = 0L;
		when(mongoRepository.updateOne(Mockito.any(Bson.class), Mockito.any(Bson.class), Mockito.any(String.class),
				Mockito.any(String.class), Mockito.eq(ChannelModel.class))).thenReturn(updatedCount);

		Throwable exception = Assertions.assertThrows(AppException.class,
				() -> channelService.delete(Mocks.userSession, requestBody, "rrId"));

		Assertions.assertEquals(exception.getMessage(), AppConstants.CHANNEL_ID_NOT_FOUND);
	}

	@Test
	public void viewChannelByStatus() throws Exception {
		Pagination requestBody = new Pagination();
		List<ChannelModel> channelModels = new ArrayList<ChannelModel>();
		ChannelModel channelModel = new ChannelModel(Mocks.CLIENT_ID, Collections.emptyList(), Collections.emptyList(),
				"general", "general channel", "1234834b5019-6730-4b11-b8b9-fe0803b0d4dc", Mocks.ACTIVE, null,
				Mocks.CM_DM_SERVICE);

		channelModels.add(channelModel);
		Mockito.when(
				mongoRepository.filterDocuments(any(Bson.class), anyInt(), anyInt(), anyString(), anyString(), any()))
				.thenReturn(channelModels);

		Assertions.assertEquals(channelService.viewChannels(Mocks.CLIENT_ID, requestBody, Mocks.RR_ID),
				channelModels);
	}
	
	@Test
	public void getChannel() throws Exception {
		Notification notification = new Notification(Mocks.CLIENT_ID, Collections.singletonList(Mocks.USER_ID), Mocks.CM_DM_SERVICE,
				"Your request has been approved", "channel-msg");
		ChannelModel channelModel = new ChannelModel();
		channelModel.setChannelId("12345");

		Mockito.when(mongoRepository.findOne(any(Bson.class), anyString(), anyString(), any()))
				.thenReturn(channelModel);
		Assertions.assertEquals(channelService.getChannel(notification).getChannelId(), channelModel.getChannelId());
	}
}
