package com.datafoundry.raven.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.datafoundry.raven.Mocks;
import com.datafoundry.raven.entity.ChannelModel;
import com.datafoundry.raven.entity.NotificationModel;
import com.datafoundry.raven.models.Notification;
import com.datafoundry.raven.models.User;
import com.datafoundry.raven.models.UserGroupRequest;
import com.datafoundry.raven.models.UserGroupResponse;
import com.datafoundry.raven.models.UserGroupResponseData;
import com.datafoundry.raven.repositories.MongoRepository;
import com.datafoundry.raven.service.impl.AsyncNotificationServiceImpl;
import com.datafoundry.raven.service.impl.RestService;
import com.datafoundry.raven.utils.AppConstants;

@ExtendWith(MockitoExtension.class)
public class AsyncNotificationServiceTest {

	@Mock
	MongoRepository<NotificationModel> mongoRepository;
	@Mock
	RestService restService;

	@Spy
	@InjectMocks
	AsyncNotificationService service = new AsyncNotificationServiceImpl();

	@Test
	public void updateAllNotification_Success() {
		service.updateAllNotificationStatusByChannelId(Mocks.userSession, "1234", "123", "123");
		verify(service, times(1)).updateAllNotificationStatusByChannelId(Mocks.userSession, "1234", "123", "123");
	}

	@Test
	public void createChannelNotifications() throws InterruptedException {
		Notification notification = new Notification(Mocks.CLIENT_ID, Collections.singletonList(Mocks.USER_ID), Mocks.CM_DM_SERVICE,
				"Your request has been approved", "channel-msg");
		ChannelModel channelModel = new ChannelModel(Mocks.CLIENT_ID, Collections.emptyList(), Collections.emptyList(),
				"general", "general channel", "1234834b5019-6730-4b11-b8b9-fe0803b0d4dc", Mocks.ACTIVE, null,
				Mocks.CM_DM_SERVICE);
		service.createChannelNotifications(notification, channelModel);
		Mockito.verify(service, times(1)).createChannelNotifications(notification, channelModel);
	}

	@Test
	public void insertUserGroupNotifications() {
		Notification notification = new Notification(Mocks.CLIENT_ID, Collections.singletonList(Mocks.USER_ID), Mocks.CM_DM_SERVICE,
				"Your request has been approved", "channel-msg");
		Set<String> userIds = new TreeSet<>();
		userIds.add("333");

		User user = new User();
		user.setUserId("333");

		List<User> listOfUsers = new ArrayList<User>();
		listOfUsers.add(user);

		UserGroupResponseData userDetails = new UserGroupResponseData();
		userDetails.setListOfUsers(listOfUsers);

		List<UserGroupResponseData> data = new ArrayList<UserGroupResponseData>();
		data.add(userDetails);

		UserGroupResponse userGrpRes = new UserGroupResponse();
		userGrpRes.setData(data);

		UserGroupRequest usrGrpReq = new UserGroupRequest();
		usrGrpReq.setUserGroups(Arrays.asList("1234_7695"));

		Mockito.when(restService.getResponse(any(), any(), any(), any(), any(), any()))
				.thenReturn(new ResponseEntity<>(userGrpRes, HttpStatus.OK));

		Assertions.assertEquals(service.getUserGroupUserIds(Arrays.asList("1234_7695"), notification),
				userIds);
	}

	@Test
	public void saveNotificationsForUserIds() {
		List<NotificationModel> notificationModels = new ArrayList<NotificationModel>();
		Notification notification = new Notification();
		NotificationModel notificationModel = new NotificationModel(new ObjectId(),
				"1234834b5019-6730-4b11-b8b9-fe0803b0d4dc", Mocks.CLIENT_ID, Mocks.USER_ID, Mocks.CM_DM_SERVICE,
				"Your case is created successfuly with the id 1234-2048-3856", "channel-msg", AppConstants.UN_DELIVERED,
				null, null, null, null, Mocks.CM_DM_SERVICE);
		notificationModels.add(notificationModel);
		Set<String> userIds = new TreeSet<>();
		Mockito.when(mongoRepository.saveMany(any(), any(), any(), any())).thenReturn(true);
		Assertions.assertTrue(service.saveNotificationsForUserIds(notification, userIds));
	}

	@Test
	public void deleteExpiredAsyncSuccess() {
		List<String> clientIdList1 = new ArrayList<>();
		service.updateExpiredChannel(clientIdList1, AppConstants.RR_ID);
		verify(service, times(1)).updateExpiredChannel(clientIdList1, AppConstants.RR_ID);
	}
}
