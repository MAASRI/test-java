package com.datafoundry.raven.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.datafoundry.raven.Mocks;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.models.Pagination;
import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.utils.AppConstants;
import com.datafoundry.raven.utils.SwaggerConstants;
import com.datafoundry.raven.utils.Utility;

/**
 * @author srilakshmi
 * @version number Raven v1.0
 * @created on 08/01/2021
 * @description Class for testing UtilityTest
 */
@ExtendWith(MockitoExtension.class)

public class UtilityTest {

	@Test
	public void testValidateUUIDs() throws AppException {
		List<String> uuIdList = new ArrayList<String>();
		uuIdList.add(Mocks.RR_ID);
		Utility.validateUUIDs(uuIdList);
		uuIdList.add("1234");
		 Throwable exception = Assertions.assertThrows(AppException.class, () -> Utility.validateUUIDs(uuIdList));
	        Assertions.assertEquals(exception.getMessage(), AppConstants.INVALID_UUID);

	}

	@Test
	public void testCreateNotificationId() throws AppException {
		String notificationId = Utility.createNotificationId(Mocks.CLIENT_ID);
		Assertions.assertEquals(notificationId.startsWith(Mocks.CLIENT_ID), Mocks.CLIENT_ID.equals(Mocks.CLIENT_ID));
	}

	@Test
	public void testGetExceptionMessage() throws AppException {
		String errorMsg = Utility.getExceptionMessage(new NullPointerException());
		Assertions.assertEquals(true, errorMsg.startsWith("java.lang.NullPointerException"));

	}

	@Test
	public void testGetRRId() throws AppException {

		Assertions.assertEquals(Mocks.RR_ID, Utility.getRRId(Mocks.RR_ID));
		String rrId = Utility.getRRId(Mocks.RR_ID);
		Assertions.assertEquals(36, rrId.length());
		 rrId = Utility.getRRId(null);
		Assertions.assertEquals(36, rrId.length());

	}

	@Test
	public void testRandomInt() throws AppException {
		String rrId = Utility.randomInt();
		Assertions.assertEquals(4, rrId.length());

	}

	@Test
	public void testGetChannelId() throws AppException {
		String channelId = Utility.getChannelId(Mocks.CLIENT_ID);
		Assertions.assertEquals(Mocks.CLIENT_ID.length() + 4, channelId.length());
	}

	@Test
	public void testGetUTCLocalDateTime() throws AppException {
		LocalDateTime date = Utility.getUTCLocalDateTime();
		Assertions.assertEquals(date.getDayOfYear(), LocalDate.now(ZoneOffset.UTC).getDayOfYear());
	}

	@Test
	public void testInsertResponse() throws AppException {
		Status status = Utility.insertResponse(true);
		Assertions.assertEquals(Utility.INSERT_RESPONSE, status);
		status = Utility.insertResponse(false);
		Assertions.assertEquals(Utility.ERROR_RESPONSE, status);
	}

	@Test
	public void testFindOffSet() throws AppException {
		Pagination pagination = new Pagination();
		pagination.setPageNumber(1);

		pagination.setPageSize(2);
		Integer skip = Utility.findOffSet(pagination);
		Assertions.assertEquals(0, skip);
	}

	@Test
	public void testGetPageNumber() throws AppException {
		Pagination pagination = new Pagination();
		Utility.getPageNumber(pagination);
		Assertions.assertEquals(1, pagination.getPageNumber());

	}

	@Test
	public void testGetPageSize() throws AppException {
		Pagination pagination = new Pagination();
		Utility.getPageSize(pagination);
		Assertions.assertEquals(10, pagination.getPageSize());

	}

	@Test
	public void testStatus() throws AppException {
		Pagination pagination = new Pagination();
		Utility.validateStatus(pagination,AppConstants.NOTIFICATION_STATUS_LIST);
		pagination.setStatus( Arrays.asList("invalid"));
		 Throwable exception = Assertions.assertThrows(AppException.class, () -> Utility.validateStatus(pagination, AppConstants.NOTIFICATION_STATUS_LIST));
	        Assertions.assertEquals(exception.getMessage(),SwaggerConstants.UNPROCESSABLE_ENTITY);
		
	

	}

}