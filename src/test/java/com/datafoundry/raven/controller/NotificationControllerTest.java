package com.datafoundry.raven.controller;

import static com.datafoundry.raven.utils.AppConstants.RR_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.datafoundry.raven.Mocks;
import com.datafoundry.raven.entity.NotificationModel;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.models.Pagination;
import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.models.UserSession;
import com.datafoundry.raven.service.NotificationService;
import com.datafoundry.raven.utils.AppConstants;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * @author akhilesh
 * @version number Raven v1.0
 * @reference https://reflectoring.io/spring-boot-web-controller-test/
 * @created on 18/12/2020
 * @description Class for testing NotificationController
 */

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = NotificationController.class)
public class NotificationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    NotificationService notificationService;
    
    @Autowired
    ObjectMapper objectMapper;

	
    @Test
    void whenValidInput_thenReturns201() throws Exception {
    	UserSession userSession = new UserSession();
		userSession.setClientId(Mocks.CLIENT_ID);
		userSession.setUserId(Mocks.USER_ID);

    	mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.CREATE_PATH).contentType(MediaType.APPLICATION_JSON)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
				.header(AppConstants.USER_SESSION, objectMapper.writeValueAsString(userSession))
                .content(Mocks.CREATE_NOTIFICATION_REQUEST)).andExpect(status().isCreated());
    }

	@Test
	void WhenCreateServiceCalledForChannelNotification_thenReturn201() throws Exception {		
		mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.CREATE_PATH)
				.contentType(MediaType.APPLICATION_JSON).header(AppConstants.RR_ID, Mocks.RR_ID)
				.content(Mocks.CREATE_NOTIFICATION_REQUEST_CHANNEL)).andExpect(status().isCreated());
	}

	@Test
	void whenCreateServiceCalledForChannelWithOutUserSession_thenReturn201() throws Exception {
		mockMvc.perform(
				post(AppConstants.NOTIFICATION_PATH + AppConstants.CREATE_PATH).contentType(MediaType.APPLICATION_JSON)
						.header(AppConstants.RR_ID, Mocks.RR_ID).content(Mocks.CREATE_NOTIFICATION_REQUEST_CHANNEL))
				.andExpect(status().isCreated());
	}

	@Test
	void whenCreateServiceCalledForChannelWithOutChannelId_thenReturn422() throws Exception {
		mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.CREATE_PATH)
				.contentType(MediaType.APPLICATION_JSON).header(AppConstants.RR_ID, Mocks.RR_ID)
				.content(Mocks.CREATE_NOTIFICATION_REQUEST_CHANNEL_WITHOUTCHANNELID))
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
    void whenPassingInvalidData_thenReturns422() throws Exception {
        mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.CREATE_PATH).contentType(MediaType.APPLICATION_JSON)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
                .content(Mocks.INVALID_CREATE_NOTIFICATION_REQUEST)).andExpect(status().isUnprocessableEntity());
    }

	@Test
    void whenMessageTypeUserMsgPassedWithOutUserId_thenReturns400() throws Exception {
        mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.CREATE_PATH).contentType(MediaType.APPLICATION_JSON)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
                .content(Mocks.WITHOUT_USERID_CREATE_NOTIFICATION_REQUEST)).andExpect(status().isBadRequest());
    }

    @Test
    void whenInvalidDataTypeIsPassedInRequest_thenReturns422() throws Exception {
        mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.CREATE_PATH).contentType(MediaType.APPLICATION_JSON)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
                .content(Mocks.INVALID_DATATYPE_NOTIFICATION_REQUEST)).andExpect(status().isBadRequest());
    }

    @Test
    void whenMessageTypeIsIncorrect_thenReturns422() throws Exception {
        mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.CREATE_PATH).contentType(MediaType.APPLICATION_JSON)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
                .content(Mocks.INCORRECT_MESSAGETYPE_CREATE_NOTIFICATION_REQUEST)).andExpect(status().isUnprocessableEntity());
    }
    
    @Test
    void whenMessageIsEmpty_thenReturns422() throws Exception {
        mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.CREATE_PATH).contentType(MediaType.APPLICATION_JSON)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
                .content(Mocks.INCORRECT_MESSAGE_CREATE_NOTIFICATION_REQUEST)).andExpect(status().isUnprocessableEntity());
    }
    
    @Test
    void whenRequestingServiceIsEmpty_thenReturns422() throws Exception {
        mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.CREATE_PATH).contentType(MediaType.APPLICATION_JSON)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
                .content(Mocks.INCORRECT_REQUESTINHSERVICE_CREATE_NOTIFICATION_REQUEST)).andExpect(status().isUnprocessableEntity());
    }    
       
    @Test
    public void whenValidDataPassed_thenReturn200() throws Exception {
        NotificationModel notificationmodel = new NotificationModel();
        notificationmodel.setClientId(Mocks.CLIENT_ID);

        List<NotificationModel> notificationmodels = new ArrayList<>();
        notificationmodels.add(notificationmodel);

        Mockito.when(notificationService.readNotificationsByStatus(any(), any(), anyString())).thenReturn(notificationmodels);

        mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.READ_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(AppConstants.USER_SESSION, Mocks.READ_NOTIFICATION_REQUEST)
                .header(RR_ID, Mocks.RR_ID)
                .content(Mocks.WITHOUT_STATUS_NOTIFICATION_REQUEST)).andExpect(status().isOk());
    }

    @Test
    public void whenNoDataFound_thenReturn204() throws Exception {
        mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.READ_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(AppConstants.USER_SESSION, Mocks.READ_NOTIFICATION_REQUEST).content("{}")
                .header(RR_ID, Mocks.RR_ID))
                .andExpect(status().isNoContent());
    }

    @Test
    public void whenIncorrectStatusPassed_thenReturn422() throws Exception {
        mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.READ_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(AppConstants.USER_SESSION, Mocks.READ_NOTIFICATION_REQUEST)
                .header(RR_ID, Mocks.RR_ID)
                .content(Mocks.WITH_INCORRECT_STATUS_NOTIFICATION_REQUEST))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void whenNotPassingHeader_thenReturn400() throws Exception {
        mockMvc.perform(post(AppConstants.NOTIFICATION_PATH + AppConstants.READ_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isBadRequest());
    }
   
    @Test
    public void whenUnDeliveredNotificationsFound_thenReturns200() throws Exception {
        UserSession userSession = new UserSession();
        userSession.setClientId(Mocks.CLIENT_ID);
        userSession.setUserId(Mocks.USER_ID);
        Pagination pagination = new Pagination(null,"1234", 1, 5 );
        mockMvc.perform(
                MockMvcRequestBuilders.post(AppConstants.NOTIFICATION_PATH + AppConstants.GET_NOTIFICATION)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(pagination))
                        .header(AppConstants.USER_SESSION, objectMapper.writeValueAsString(userSession)).header(AppConstants.RR_ID, Mocks.RR_ID))
                .andExpect(status().is2xxSuccessful());

    }

    @Test
    void whenNoUnDeliveredNotificationsNotFound_thenReturns204() throws Exception {
        UserSession userSession = new UserSession();
        userSession.setClientId(Mocks.CLIENT_ID);
        userSession.setUserId(Mocks.USER_ID);
        Pagination pagination = new Pagination(null,"1234", 1, 5);
        mockMvc.perform(
                MockMvcRequestBuilders.post(AppConstants.NOTIFICATION_PATH + AppConstants.GET_NOTIFICATION)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(pagination))
                        .header(AppConstants.USER_SESSION, objectMapper.writeValueAsString(userSession)).header(AppConstants.RR_ID, Mocks.RR_ID))
                .andExpect(status().isNoContent());
    }

    @Test
    void whenPassingInvalidUserIdForNotifications_thenReturns204() throws Exception {
        UserSession userSession = new UserSession();
        userSession.setClientId(Mocks.CLIENT_ID);
        userSession.setUserId("7865875");
        Pagination pagination = new Pagination(null,"1234", 1, 5);

        mockMvc.perform(
                MockMvcRequestBuilders.post(AppConstants.NOTIFICATION_PATH + AppConstants.GET_NOTIFICATION)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(pagination))
                        .header(AppConstants.USER_SESSION, objectMapper.writeValueAsString(userSession)).header(AppConstants.RR_ID, Mocks.RR_ID))
                .andExpect(status().isNoContent());
    }

    @Test
    void whenPassingWithoutUsersessionForNotifications_thenReturns400() throws Exception {
        Pagination pagination = new Pagination(null,"1234", 1, 5);
        mockMvc.perform(
                MockMvcRequestBuilders.post(AppConstants.NOTIFICATION_PATH + AppConstants.GET_NOTIFICATION)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(pagination))
                        .header(AppConstants.RR_ID, Mocks.RR_ID))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenAsynchcallFails_thenReturns200() throws Exception {
        Pagination pagination = new Pagination(null, "1234",1, 5);
        mockMvc.perform(
                MockMvcRequestBuilders.post(AppConstants.NOTIFICATION_PATH + AppConstants.GET_NOTIFICATION)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(pagination))
                        .header(AppConstants.RR_ID, Mocks.RR_ID))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenValidInputForUpdateRequest_thenReturns200() throws Exception {
        UserSession userSession = new UserSession("John", "Ch", false, "1234", "dfAdmin", "John", "seven", "johnxyz@df.com", "12345678");
        Mockito.when(notificationService.statusUpdate(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(new Status.StatusBuilder("", true, 200).build());
        mockMvc.perform(put(AppConstants.NOTIFICATION_PATH + AppConstants.UPDATE_PATH).contentType(MediaType.APPLICATION_JSON)
                .header(AppConstants.USER_SESSION, objectMapper.writeValueAsString(userSession))
                .header(RR_ID, Mocks.RR_ID)
                .content(Mocks.UPDATE_NOTIFICATION_REQUEST)).andExpect(status().isOk());
    }

    @Test
    void whenInValidNotificationIdForUpdateRequest_thenReturns404() throws Exception {
        Mockito.when(notificationService.statusUpdate(Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(new AppException(AppConstants.NOTIFICATION_ID_NOT_FOUND, AppConstants.NOTIFICATION_ID_NOT_FOUND));
        mockMvc.perform(put(AppConstants.NOTIFICATION_PATH + AppConstants.UPDATE_PATH).contentType(MediaType.APPLICATION_JSON)
                .header(AppConstants.USER_SESSION, objectMapper.writeValueAsString(Mocks.userSession))
                .header(RR_ID, Mocks.RR_ID)
                .content(Mocks.INVALID_UPDATE_NOTIFICATION_ID_REQUEST)).andExpect(status().isNotFound());
    }

    @Test
    void whenInvalidUserSessionPassedForUpdateRequest_thenReturns400() throws Exception {
        UserSession userSession = new UserSession();
        mockMvc.perform(put(AppConstants.NOTIFICATION_PATH + AppConstants.UPDATE_PATH).contentType(MediaType.APPLICATION_JSON)
                .header(AppConstants.USER_SESSION, objectMapper.writeValueAsString(userSession))
                .header(RR_ID, Mocks.RR_ID)
                .content(Mocks.UPDATE_NOTIFICATION_REQUEST)).andExpect(status().isBadRequest());
    }

    @Test
    void whenInvalidStatusIsPassedInRequestBodyForUpdateRequest_thenReturns422() throws Exception {
        mockMvc.perform(put(AppConstants.NOTIFICATION_PATH + AppConstants.UPDATE_PATH).contentType(MediaType.APPLICATION_JSON)
                .header(AppConstants.USER_SESSION, objectMapper.writeValueAsString(Mocks.userSession))
                .header(RR_ID, Mocks.RR_ID)
                .content(Mocks.INVALID_UPDATE_STATUS_NOTIFICATION_REQUEST)).andExpect(status().isUnprocessableEntity());
    }

    @Test
    void whenDeleteExpiredNotifications_thenReturn200() throws Exception {
        mockMvc.perform(put(AppConstants.NOTIFICATION_PATH + AppConstants.UPDATE_EXPIRED).contentType(MediaType.APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)).andExpect(status().isOk());
    }

}
