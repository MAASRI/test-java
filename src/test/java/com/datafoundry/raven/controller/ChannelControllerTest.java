package com.datafoundry.raven.controller;

import static com.datafoundry.raven.utils.AppConstants.CHANNEL_ID_NOT_FOUND;
import static com.datafoundry.raven.utils.AppConstants.CHANNEL_PATH;
import static com.datafoundry.raven.utils.AppConstants.CREATE_PATH;
import static com.datafoundry.raven.utils.AppConstants.DELETE_PATH;
import static com.datafoundry.raven.utils.AppConstants.RR_ID;
import static com.datafoundry.raven.utils.AppConstants.UPDATE_PATH;
import static com.datafoundry.raven.utils.AppConstants.USER_SESSION;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.datafoundry.raven.Mocks;
import com.datafoundry.raven.entity.ChannelModel;
import com.datafoundry.raven.exception.AppException;
import com.datafoundry.raven.models.Channel;
import com.datafoundry.raven.models.ChannelUpdate;
import com.datafoundry.raven.models.Status;
import com.datafoundry.raven.models.UserSession;
import com.datafoundry.raven.service.ChannelService;
import com.datafoundry.raven.utils.AppConstants;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author ram
 * @version number Raven v1.0
 * @created 23/12/2020
 * @description Class for testing ChannelController
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ChannelController.class)
public class ChannelControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ChannelService channelService;

    @Autowired
    ObjectMapper objectMapper;

    @Tag("createChannel")
    @Test
    void whenNoUserGroupAndUsers_thenReturns201() throws Exception {
        Channel channel = new Channel();
        channel.setClientId(Mocks.CLIENT_ID);
        channel.setRequestingService(Mocks.MICROSERVICE);
        channel.setChannelName("general");
        channel.setDescription("General channel for all users");


        mockMvc.perform(post(CHANNEL_PATH + CREATE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .content(objectMapper.writeValueAsString(channel))).andExpect(status().isCreated());
    }

    @Tag("createChannel")
    @Test
    void whenValidInput_thenReturns201() throws Exception {
        Channel channel = new Channel();
        channel.setClientId(Mocks.CLIENT_ID);
        channel.setRequestingService(Mocks.MICROSERVICE);
        channel.setChannelName("general");
        channel.setDescription("General channel for all users");
        channel.setUserGroup(Arrays.asList("dev", "qa"));
        channel.setUsers(Arrays.asList("user1", "user2"));


        mockMvc.perform(post(CHANNEL_PATH + CREATE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .content(objectMapper.writeValueAsString(channel))).andExpect(status().isCreated());
    }

    @Tag("createChannel")
    @Test
    void whenMissingMandatoryParameter_thenReturns400() throws Exception {
        Channel channel = new Channel();
        channel.setClientId(Mocks.CLIENT_ID);
        channel.setChannelName("general");
        channel.setDescription("General channel for all users");

        mockMvc.perform(post(CHANNEL_PATH + CREATE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .content(objectMapper.writeValueAsString(channel))).andExpect(status().isBadRequest());
    }

    @Test
    void whenBGSRequestWithoutUserSession_thenReturns201() throws Exception {
        Channel channel = new Channel();
        channel.setClientId(Mocks.CLIENT_ID);
        channel.setRequestingService(Mocks.UI_SERVICE);
        channel.setChannelName("general");
        channel.setDescription("General channel for all users");
        channel.setUserGroup(Arrays.asList("dev", "qa"));
        channel.setUsers(Arrays.asList("user1", "user2"));

        mockMvc.perform(post(CHANNEL_PATH + CREATE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .content(objectMapper.writeValueAsString(channel))).andExpect(status().isCreated());
    }

    @Tag("createChannel")
    @Test
    void whenBGSRequestWithUserSession_thenReturns201() throws Exception {

        UserSession userSession = new UserSession();
        userSession.setClientId(Mocks.CLIENT_ID);
        userSession.setUserId(Mocks.USER_ID);


        Channel channel = new Channel();
        channel.setRequestingService(Mocks.UI_SERVICE);
        channel.setChannelName("general");
        channel.setDescription("General channel for all users");
        channel.setUserGroup(Arrays.asList("dev", "qa"));
        channel.setUsers(Arrays.asList("user1", "user2"));


        mockMvc.perform(post(CHANNEL_PATH + CREATE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .header(USER_SESSION, objectMapper.writeValueAsString(userSession))
                .content(objectMapper.writeValueAsString(channel))).andExpect(status().isCreated());
    }

    // Update Channel Tests
    @Tag("updateChannel")
    @DisplayName("Users is added to channel with add action")
    @Test
    void whenBGSUpdateAddUsersToChannel_thenReturns200() throws Exception {


        UserSession userSession = new UserSession();
        userSession.setClientId(Mocks.CLIENT_ID);
        userSession.setUserId(Mocks.USER_ID);


        ChannelUpdate channelUpdate = new ChannelUpdate();
        channelUpdate.setChannelId("12345678");
        channelUpdate.setAction("add");
        channelUpdate.setUsers(Arrays.asList("user1", "user2"));


        mockMvc.perform(put(CHANNEL_PATH + UPDATE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .header(USER_SESSION, objectMapper.writeValueAsString(userSession))
                .content(objectMapper.writeValueAsString(channelUpdate))).andExpect(status().isOk());
    }

    @Tag("updateChannel")
    @DisplayName("UserGroup's is removed from channel with add remove")
    @Test
    void whenBGSUpdateRemoveUserGroupFromChannel_thenReturns200() throws Exception {


        UserSession userSession = new UserSession();
        userSession.setClientId(Mocks.CLIENT_ID);
        userSession.setUserId(Mocks.USER_ID);


        ChannelUpdate channelUpdate = new ChannelUpdate();
        channelUpdate.setChannelId("12345678");
        channelUpdate.setAction("remove");
        channelUpdate.setUserGroup(Arrays.asList("dev", "qa"));


        mockMvc.perform(put(CHANNEL_PATH + UPDATE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .header(USER_SESSION, objectMapper.writeValueAsString(userSession))
                .content(objectMapper.writeValueAsString(channelUpdate))).andExpect(status().isOk());
    }

    @Tag("updateChannel")
    @DisplayName("Channel description updated if userSession header is passed")
    @Test
    void whenBGSUpdateRequestWithUserSession_thenReturns200() throws Exception {


        UserSession userSession = new UserSession();
        userSession.setClientId(Mocks.CLIENT_ID);
        userSession.setUserId(Mocks.USER_ID);


        ChannelUpdate channelUpdate = new ChannelUpdate();
        channelUpdate.setChannelId("12345678");
        channelUpdate.setDescription("General channel for all users");


        mockMvc.perform(put(CHANNEL_PATH + UPDATE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .header(USER_SESSION, objectMapper.writeValueAsString(userSession))
                .content(objectMapper.writeValueAsString(channelUpdate))).andExpect(status().isOk());
    }

    @Tag("updateChannel")
    @DisplayName("Channel not updated if nothing to update")
    @Test
    void whenBGSUpdateRequestWithNoUpdate_thenReturns400() throws Exception {


        UserSession userSession = new UserSession();
        userSession.setClientId(Mocks.CLIENT_ID);
        userSession.setUserId(Mocks.USER_ID);


        ChannelUpdate channelUpdate = new ChannelUpdate();
        channelUpdate.setChannelId("12345678");


        mockMvc.perform(put(CHANNEL_PATH + UPDATE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .header(USER_SESSION, objectMapper.writeValueAsString(userSession))
                .content(objectMapper.writeValueAsString(channelUpdate))).andExpect(status().isBadRequest());
    }

    @Tag("updateChannel")
    @DisplayName("Channel not updated without userSession")
    @Test
    void whenBGSUpdateRequestWithoutUserSession_thenReturns400() throws Exception {

        ChannelUpdate channelUpdate = new ChannelUpdate();
        channelUpdate.setChannelId("12345678");
        channelUpdate.setDescription("General channel for all users");
        channelUpdate.setUserGroup(Arrays.asList("dev", "qa"));
        channelUpdate.setUsers(Arrays.asList("user1", "user2"));


        mockMvc.perform(put(CHANNEL_PATH + UPDATE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .content(objectMapper.writeValueAsString(channelUpdate))).andExpect(status().isBadRequest());
    }


    @Test
    void whenValidInput_thenReturn200() throws Exception {
        String requestBody = "{\"channelId\":\"12345678\"}";
        Mockito.when(channelService.delete(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(new Status.StatusBuilder("", true, 200).build());
        mockMvc.perform(put(CHANNEL_PATH + DELETE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .header(AppConstants.USER_SESSION, objectMapper.writeValueAsString(Mocks.userSession))
                .content(requestBody)).andExpect(status().isOk());
    }

    @Test
    void whenInvalidChannelId_thenReturn404() throws Exception {
        String requestBody = "{\"channelId\":\"12345678\"}";
        Mockito.when(channelService.delete(Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(new AppException(CHANNEL_ID_NOT_FOUND, CHANNEL_ID_NOT_FOUND));
        mockMvc.perform(put(CHANNEL_PATH + DELETE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .header(AppConstants.USER_SESSION, objectMapper.writeValueAsString(Mocks.userSession))
                .content(requestBody)).andExpect(status().isNotFound());
    }

    @Test
    void whenInValidUserSession_thenReturn400() throws Exception {
        UserSession userSession = new UserSession();
        String userSessionJsonString = objectMapper.writeValueAsString(userSession);
        String requestBody = "{\"channelId\":\"12345678\"}";
        mockMvc.perform(put(CHANNEL_PATH + DELETE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .header(AppConstants.USER_SESSION, userSessionJsonString)
                .content(requestBody)).andExpect(status().isBadRequest());
    }

    @Test
    void whenUnprocessableEntity_thenReturn422() throws Exception {
        String requestBody = "{\"channelId\":\"12345678\"}";
        Mockito.when(channelService.delete(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenThrow(new AppException(AppConstants.UNPROCESSABLE_ENTITY, AppConstants.CHANNEL_ID_NOT_FOUND));
        mockMvc.perform(put(CHANNEL_PATH + DELETE_PATH).contentType(APPLICATION_JSON)
                .header(RR_ID, Mocks.RR_ID)
                .header(AppConstants.USER_SESSION, objectMapper.writeValueAsString(Mocks.userSession))
                .content(requestBody)).andExpect(status().isUnprocessableEntity());
    }

    @Test
	public void whenValidDataPassed_thenReturn200() throws Exception {
		ChannelModel channelModel = new ChannelModel();
		channelModel.setClientId(Mocks.CLIENT_ID);

		List<ChannelModel> channelModels = new ArrayList<>();
		channelModels.add(channelModel);

		Mockito.when(channelService.viewChannels(any(), any(), any())).thenReturn(channelModels);

		mockMvc.perform(
				post(AppConstants.CHANNEL_PATH + AppConstants.VIEW_PATH).contentType(MediaType.APPLICATION_JSON_VALUE)
						.header(AppConstants.USER_SESSION, Mocks.USER_SESSION_EXAMPLE)
						.content(Mocks.WITHOUT_STATUS_CHANNEL_REQUEST))
				.andExpect(status().isOk());
	}

	@Test
	public void whenNoDataFound_thenReturn204() throws Exception {
		mockMvc.perform(
				post(AppConstants.CHANNEL_PATH + AppConstants.VIEW_PATH).contentType(MediaType.APPLICATION_JSON_VALUE)
						.header(AppConstants.USER_SESSION, Mocks.USER_SESSION_EXAMPLE).content("{}"))
				.andExpect(status().isNoContent());
	}

	@Test
	public void whenIncorrectStatusPassed_thenReturn422() throws Exception {
		mockMvc.perform(
				post(AppConstants.CHANNEL_PATH + AppConstants.VIEW_PATH).contentType(MediaType.APPLICATION_JSON_VALUE)
						.header(AppConstants.USER_SESSION, Mocks.USER_SESSION_EXAMPLE)
						.content(Mocks.WITH_INCORRECT_CHANNEL_STATUS__REQUEST))
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void whenNotPassingHeader_thenReturn400() throws Exception {
		mockMvc.perform(
				post(AppConstants.CHANNEL_PATH + AppConstants.VIEW_PATH).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isBadRequest());
	}

}
