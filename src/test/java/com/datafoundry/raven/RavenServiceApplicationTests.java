package com.datafoundry.raven;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author akhilesh
 * @created on 18/12/2020
 * @version number Raven v1.0
 */
@SpringBootTest
class RavenServiceApplicationTests {

	@Test
	void contextLoads() {
	}

}
