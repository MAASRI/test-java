package com.datafoundry.raven;

import com.datafoundry.raven.models.NotificationUpdate;
import com.datafoundry.raven.models.UserSession;

/**
 * @author akhilesh
 * @version number Raven v1.0
 * @created on 18/12/2020
 * @description Class for maintaining MOCK data
 */
public class Mocks {

	public static final String CM_DM_SERVICE = "cm-dm-service";
	public static final String CLIENT_ID = "3454";
	public static final String UI_SERVICE = "ui-service";

	public static final String USER_ID = "12R3323";
	public static final String MICROSERVICE = "microservice";
	public static final String RR_ID = "9c99969e-adb7-4eb0-a74b-b66501f7488e";
	public static final String USER_MSG_TYPE = "user-msg";
	public static final NotificationUpdate notificationUpdate = new NotificationUpdate("1234527a149e-2e24-4fbe-b838-dd0dc661928a","read");
	public static final UserSession userSession = new UserSession("John","Ch",false,"1234","dfAdmin","John","seven","johnxyz@df.com","12345678");
	public static final String CREATE_NOTIFICATION_REQUEST_CHANNEL = "{\"clientId\":\"0711\",\"userId\":\"048793c711\",\"requestingService\":\"cm-dm-service\",\"message\":\"Document Approved!!\",\"messageType\":\"channel-msg\",\"channelId\":\"1234_7695\"}";
	public static final String CREATE_NOTIFICATION_REQUEST_CHANNEL_WITHOUTCHANNELID = "{\"clientId\":\"0711\",\"userId\":\"048793c711\",\"requestingService\":\"cm-dm-service\",\"message\":\"Document Approved!!\",\"messageType\":\"channel-msg\"}";
	
	public static final String INVALID_DATATYPE_NOTIFICATION_REQUEST = "{\"clientId\":true,\"userId\":\"048793c711\",\"requestingService\":\"cm-dm-service\",\"message\":\"Document Approved!!\",\"messageType\":\"user-msg\"}";
	public static final String CREATE_NOTIFICATION_REQUEST = "{\"clientId\": \"7249\",\"message\": \"Your case is created successfuly with the id 1234-1201-2018\",\"messageType\": \"user-msg\",\"requestingService\": \"cm-dm-service\",\"userIds\": [\"12344038\",\"1234325\"]}";
	public static final String INVALID_CREATE_NOTIFICATION_REQUEST = "{\"clientid\":\"0711\",\"requestingService\":\"cm-dm-service\",\"message\":\"Document Approved!!\",\"messageType\":\"user-msg\"}";
	public static final String WITHOUT_USERID_CREATE_NOTIFICATION_REQUEST = "{\"clientId\": \"7249\",\"message\": \"Your case is created successfuly with the id 1234-1201-2018\",\"messageType\": \"user-msg\",\"requestingService\": \"cm-dm-service\",\"userIds\": [\" \"]}";
	public static final String INCORRECT_MESSAGETYPE_CREATE_NOTIFICATION_REQUEST = "{\"clientId\":\"0711\",\"requestingService\":\"cm-dm-service\",\"message\":\"Document Approved!!\",\"messageType\":\"user-msg23\",\"userId\":\"1233423\",\"channelId\":\"12345\"}";
	public static final String INCORRECT_MESSAGE_CREATE_NOTIFICATION_REQUEST = "{\"clientId\":\"0711\",\"requestingService\":\"cm-dm-service\",\"message\":\"\",\"messageType\":\"channel-msg\",\"userId\":\"1233423\",\"channelId\":\"12345\"}";
	public static final String INCORRECT_REQUESTINHSERVICE_CREATE_NOTIFICATION_REQUEST = "{\"clientId\":\"0711\",\"requestingService\":\"\",\"message\":\"\",\"messageType\":\"channel-msg\",\"userId\":\"1233423\",\"channelId\":\"12345\"}";

	public static final String WITHOUT_STATUS_NOTIFICATION_REQUEST = "{\"pageNumber\":\"1\", \"pageSize\":\"1\"}";
	public static final String WITH_INCORRECT_STATUS_NOTIFICATION_REQUEST = "{\"status\":[\"deliver]\"],\"pageNumber\":\"1\", \"pageSize\":\"1\"}";
	public static final String READ_NOTIFICATION_REQUEST = "{\"clientId\":\"1234\",\"userId\":\"09e105a0-93bf-41bf-8c0f-71b48793c711\",\"firstName\":\"\",\"lastName\":\" \",\"access\":\" \",\"roleId\":\" \", \"displayName\":\" \",\"roleName\":\" \", \"emailId\":\" \"}";
	public static final String INCORRECT_CREATE_NOTIFICATION_REQUEST = "{\"clientId\":\"0711\",\"userId\":\"048793c711\",\"requestingService\":\"cm-dm-service\",\"message\":\"Document Approved!!\",\"messageType\":\"user-msg\"}";
	public static final String PAGEABLE_REQUEST = "{\"pageNumber\":1,\"pageSize\":5";
	public static final String UPDATE_NOTIFICATION_REQUEST = "{\"notificationId\":\"1234f1ccebc5-f7ca-4fb2-b38a-f5b201077c17\",\"status\":\"read\"}";
	public static final String INVALID_UPDATE_NOTIFICATION_ID_REQUEST = "{\"notificationId\":\"1234\",\"status\":\"read\"}";
	public static final String INVALID_UPDATE_STATUS_NOTIFICATION_REQUEST = "{\"notificationId\":\"1234f1ccebc5-f7ca-4fb2-b38a-f5b201077c17\",\"status\":\"Read\"}";
    public static final String ACTIVE = "active";
    public static final String WITHOUT_STATUS_CHANNEL_REQUEST = "{\"pageNumber\":\"1\", \"pageSize\":\"1\"}";
	public static final String WITH_INCORRECT_CHANNEL_STATUS__REQUEST = "{\"status\":[\"live\", \"activated\"],\"pageNumber\":\"1\", \"pageSize\":\"1\"}";
	public static final Object USER_SESSION_EXAMPLE = "{\"firstName\": \"\",\"lastName\": \"\",\"access\": true,\"clientId\": \"1234\",\"roleId\": \"12341117\",\"displayName\": \"Shravan G\",\"roleName\": \"dfAdmin\",\"emailId\": \"shravan.g@datafoundry.ai\",\"userId\": \"12344510\"}";

   

}
