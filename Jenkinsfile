pipeline {
    options { 
      disableConcurrentBuilds() 
      buildDiscarder(logRotator(numToKeepStr: '5'))
    }
    environment {
    registry = '622925461941.dkr.ecr.us-east-1.amazonaws.com/raven-service'
    credentialsId = 'ecr:us-east-1:AWS_DF_KEY'
    dockerImage=''
    def mvnHome = tool name: 'maven', type: 'maven'    
    url= 'https://622925461941.dkr.ecr.us-east-1.amazonaws.com/raven-service'
    BRANCH_NAME = "${GIT_BRANCH.split("/")[1]}"
    }
    agent any    
    stages{
        stage("mvn build"){
          steps{ 
             sh "${mvnHome}/bin/mvn clean install -DskipTests"
            }
        } 
		stage('SonarQube Analysis') {
     		steps{
				sh "${mvnHome}/bin/mvn sonar:sonar"
			}
        }        
        stage("Building Docker Image"){
            steps{
               script{
                 dockerImage = docker.build registry + ":$BRANCH_NAME-$BUILD_NUMBER"
               }
            }
        }
        stage('Pushing Image to ECR') {
            steps{
               script {
                 withDockerRegistry(credentialsId: 'ecr:us-east-1:AWS_DF_KEY', url: 'https://622925461941.dkr.ecr.us-east-1.amazonaws.com/raven-service') {
                   sh 'docker push  622925461941.dkr.ecr.us-east-1.amazonaws.com/raven-service":$BRANCH_NAME-$BUILD_NUMBER"'                   
                   }
               }
            }
        }
        stage('Remove Unused docker image') {
           steps{
             sh "docker rmi -f $registry:$BRANCH_NAME-$BUILD_NUMBER"
          }
        }
        stage("Deploy to DEV"){
            when {
                branch 'dev'
            }
            steps{                
                sh "chmod +x tag.sh"
                sh "./tag.sh $BRANCH_NAME-${BUILD_NUMBER}"       
              script{ 
                sh 'export KUBECONFIG=/home/dev/.kube/config && kubectl apply -f /var/lib/jenkins/workspace/dev-raven-service/dev-raven-service.yaml'
              }
            }
            post{
                success{
                    echo "Successfully deployed to DEV"
                }
                failure{
                    echo "Failed deploying to DEV"
                }
            }
        }
        stage("Deploy to QA"){
            when {
                branch 'qa'
            }
            steps{                
                sh "chmod +x tag.sh"
                sh "./tag.sh $BRANCH_NAME-${BUILD_NUMBER}"       
              script{ 
                sh 'export KUBECONFIG=/home/qa/.kube/config && kubectl apply -f /var/lib/jenkins/workspace/qa-raven-service/qa-raven-service.yaml'
              }
            }
            post{
                success{
                    echo "Successfully deployed to QA"
                }
                failure{
                    echo "Failed deploying to QA"
                }
            }
        }
        stage("Deploy to STG"){
            when {
                branch 'stg'
            }
            steps{                
                sh "chmod +x tag.sh"
                sh "./tag.sh $BRANCH_NAME-${BUILD_NUMBER}"       
              script{ 
                sh 'export KUBECONFIG=/home/stg/.kube/config && kubectl apply -f /var/lib/jenkins/workspace/stg-raven-service/raven-service.yaml'
              }
            }
            post{
                success{
                    echo "Successfully deployed to STG"
                }
                failure{
                    echo "Failed deploying to STG"
                }
            }
        }
        stage("Deploy to SB"){
            when {
                branch 'sb'
            }
            steps{                
                sh "chmod +x tag.sh"
                sh "./tag.sh $BRANCH_NAME-${BUILD_NUMBER}"       
              script{ 
                sh 'export KUBECONFIG=/home/sbox/.kube/config && kubectl apply -f /var/lib/jenkins/workspace/sb-raven-service/sb-raven-service.yaml'
              }
            }
            post{
                success{
                    echo "Successfully deployed to SB"
                }
                failure{
                    echo "Failed deploying to SB"
                }
            }
        }
    }
}
