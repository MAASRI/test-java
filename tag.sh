#!/bin/bash
sed "s/latest/$1/g" deployment.yaml > raven-service.yaml
sed "s/latest/$1/g" dev-deployment.yaml > dev-raven-service.yaml
sed "s/latest/$1/g" qa-deployment.yaml > qa-raven-service.yaml
