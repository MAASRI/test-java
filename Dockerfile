FROM openjdk:11
VOLUME /opt
EXPOSE 9094
COPY target/raven-service-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT java -Djava.security.egd=file:/dev/./urandom -jar /app.jar